package com.np.servicefactory.util;

public enum EnvType {
	DEV("dev"),
	DEVELOPER("developer");
	
	private String typeCode;
	
	EnvType(String typeCode) {
		this.typeCode = typeCode;
	}
}
