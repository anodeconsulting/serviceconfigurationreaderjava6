package com.np.servicefactory.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import com.np.servicefactory.util.ServiceFatoryConstant;

public class UnzipUtil 
{
	List<String> fileList;
    private static final String SOURCE_ZIP_FILE = "/"+ServiceFatoryConstant.ServiceFactory_Source_Input_Zip_File;

    
    UnzipUtil()
    {
    	fileList = new ArrayList<String>();
    }
    
    public static void main(String args[])
    {
    	UnzipUtil unzipApp = new UnzipUtil();
    	//unzipApp.unZipMe(SOURCE_ZIP_FILE,TARGET_FOLDER);
    //	unzipApp.createFolder(TARGET_FOLDER);
    	System.out.println("Source file== " + SOURCE_ZIP_FILE );
    	//System.out.println("Target folder== " + TARGET_FOLDER );
    	//unzipApp.unZipIt(SOURCE_ZIP_FILE, TARGET_FOLDER);
    	
    }
    

    private void createFolder(String outputPath){
    	//First of all we need to create the target folder if it does not exist
    	File opFolder = new File(outputPath);
    	if(!opFolder.exists())
    	{
    		opFolder.mkdir();
    	}
    	System.out.println("Process Started.....");
    }
    
   
    
    public static void unZipMe(String zipFile, String targetFolder)
    {
    	byte[] buffer = new byte[1024];
    	try
    	{
	    	//First of all we need to create the target folder if it does not exist
	    	File opFolder = new File(targetFolder);
	    	if(!opFolder.exists())
	    	{
	    		opFolder.mkdir();
	    	}
	    	System.out.println("Process Started.....");
	    	//Get hold of the ZipInputStream from the zip file
	    	ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
	        //Get hold of the zip file list entries
	        ZipEntry zipEntry = zis.getNextEntry();
	        while(zipEntry != null)
	        {
	        	//Get the name of the file
	        	String fileName = zipEntry.getName();
	            File tempFile = new File(targetFolder + File.separator + fileName);
	            System.out.println("Unzipping: " + tempFile.getName());
	            //Get the parent folder of the temp file and create the folder
	            new File(tempFile.getParent()).mkdirs();
	            FileOutputStream fos = new FileOutputStream(tempFile);             
	            
	            int len;
	            while ((len = zis.read(buffer)) > 0) 
	            {
	            	fos.write(buffer, 0, len);
	            }	 
	            fos.close();   
	            zipEntry = zis.getNextEntry();	            
	        }
	        zis.closeEntry();
        	zis.close();
        	System.out.println("Process completed successfully");
    	}
    	catch(IOException ex)
    	{
    		System.out.println("Unable to process..." + ex.getMessage());
    	}
    }
    
    
    public static void unZipIt(String zipFile, String outputFolder) {

        byte[] buffer = new byte[1024];

        try {
            //create output directory is not exists
            File folder = new File(outputFolder);
            if (!folder.exists()) {
                folder.mkdir();
            }

            //get the zip file content
            ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
            //get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {
                String fileName = ze.getName();
                if (ze.isDirectory()) {
                    ze = zis.getNextEntry();
                    continue;
                }
                System.out.println("origin file: " + fileName);
                fileName = new File(fileName).getName();
                File newFile = new File(outputFolder + File.separator + fileName);
                System.out.println("file unzip : " + newFile.getAbsoluteFile());

                //create all non exists folders
                //else you will hit FileNotFoundException for compressed folder
                new File(newFile.getParent()).mkdirs();

                FileOutputStream fos = new FileOutputStream(newFile);

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                ze = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();

            System.out.println("Done");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}