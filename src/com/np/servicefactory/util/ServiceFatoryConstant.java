/**
 * 
 */
package com.np.servicefactory.util;

/**
 * @author Wang
 * Define all the project related Constants value
 */
public class ServiceFatoryConstant {
	
	public static String ServiceFactory_Source_Input_Zip_File="export.zip";
	public static String ServiceFactory_Target_Output_Zip_File="serviceFactoryProject.zip";
	
	public static String ServiceFactory_XSD_Type=".xsd";
	public static String ServiceFactory_WSDL_Type="wsdl";
	
	public static String SF_DEPLOYMENT_MANIFEST_SERVICE_UT_File="deployment_manifest_service_ut.xml";
	public static String SF_DEPLOYMENT_MANIFEST_File="deployment_manifest.xml";
	public static String SF_DEPLOYMENT_MANIFEST_UT_File="deployment_manifest_ut.xml";
	
	
	public static String ServiceFactory_ServiceConfig_File="ServiceConfig.xml";
	public static String ServiceFactory_ResponderConfig_File="ResponderConfg.xml";
	public static String ServiceFactory_AAA_CONFIG_FILE="AAAInfo.xml";
	public static String ServiceFactory_SG_CONFIG_XML = "SGConfig.xml";
	
	public static String ServiceFactory_DATA_VALIDATION_CONFIG_FILE="DataValidatorConfig.xml";
	public static String ServiceFactory_LOG_CONFIG_FILE="LogConfig.xml";
	
	public static String ServiceFactory_Source_WSDL_SchemaLocation_Key= "schemalocation";
	public static String ServiceFactory_Config_File_Status= "enabled";
	
	public static String ServiceFactory_Target_DEV_Build_File= "C:/Workspace/Project/NextPathWay/IBIT/Developement1/Leap Service Factory/UpdatedPropertiesFile/build.dev.properties";
	
	
	
	//deployment manifest constants
	
	public static String INFRA_EXTRACT_PAYLOAD_XSLT = "extract-payload.xslt";
	public static String INFRA_FORMAT_ERROR_LOG_MSG_XSLT = "format-error-log-message.xslt";
	public static String INFRA_FORMAT_LOG_MSG_XSLT = "format-log-message.xslt";
	public static String INFRA_GENERIC_SOAP_FAULT_XSLT = "generic-soap-fault-xml.xslt";
	public static String INFRA_GENERIC_SOAP_FAULT_BIN_XSLT = "generic-soap-fault-bin.xslt";
	public static String INFRA_RETRIEVE_SOAP_ACTION_VAR_XSLT = "retrieve-soap-action-var.xslt";
	public static String INFRA_SET_SOAP_ACTION_VAR_XSLT = "set-soap-action-var.xslt";
	public static String INFRA_WRAP_PAYLOAD_XSLT = "wrap-payload.xslt";
	public static String INFRA_IDENTITY_XSLT="identity.xslt";
	public static String INFRA_AAA_POLICY_SWITCHER_XSLT = "aaa-policy-switcher.xslt";
	public static String INFRA_CUSTOM_ERROR_HANDLER_XSLT = "custom-error-handler.xslt";
	public static String INFRA_SF_UTILITY_XSLT = "servicefactory-utility.xslt";
	public static String INFRA_EXTRACT_IDENTITY_XSLT = "extract-identity.xslt";
	public static String INFRA_VALIDATE_CONSUMER_DATA_XSLT = "validate-consumer-data.xslt";
	public static String INFRA_CONVERT_BIN_XSLT = "convert-bin.xslt";
	public static String INFRA_EXE_PCI_COMPLIANCE_XSLT="execute-pci-compliance.xslt";
	public static String INFRA_ASYNC_LOG_SENDER_XSLT = "async-log-sender.xslt";
	
	public static String INFRA_SG_BUILD_SCATTER_URLS_XSLT="build-scatter-urls.xslt";
	public static String INFRA_SG_CONDITION_BUILDER_XSLT="condition-builder.xslt";
	public static String INFRA_SG_CONTEXT_SWITCHER_BATCH_XSLT="context-switcher-batch.xslt";
	public static String INFRA_SG_CONTEXT_SWITCHER_SERIAL_XSLT="context-switcher-serial.xslt";
	public static String INFRA_SG_POSTACTION_EVALUATOR_XSLT="postaction-evaluator.xslt";
	public static String INFRA_SG_SERVICE_CALLER_POSTACTION_XSLT="service-caller-postaction.xslt";
	public static String INFRA_SG_SERVICE_CALLER_PREACTION_XSLT="service-caller-preaction.xslt";
	public static String INFRA_SG_SET_SG_CONFIG_VAR_XSLT="set-sg-config-var.xslt";
	public static String INFRA_SG_BATCH_PATH_EVALUATOR_XSLT="batch-path-evaluator.xslt";
	
	
	public static String CERT_IBIT_SF_PRIVATE_KEY = "IBITServiceFactory-privkey.pem";
	public static String CERT_IBIT_SF_SSCERT = "IBITServiceFactory-sscert.pem";
	public static String CERT_SECURE_SERVICE_FACTORY_CERT = "sfdevelopment-cert.pem";
	public static String CERT_SECURE_SERVICE_FACTORY_PRIVATE_KEY = "sfdevelopment-privkey.pem";
	
	public static String INFRA_SET_ODBC_VAR_XSLT = "set-odbc-var.xslt";
	public static String INFRA_QUERY_RESPONDER_XSLT = "query-responder.xslt";
	
	public static String INFRA_SET_RESPONSE_PLAYLOAD_XML_XSLT = "set-response-payload-xml.xslt";
	public static String OPERATION_PATTERN_BINARY = "binary";
	public static String OPERATION_PATTERN_ODBC = "odbc";
	public static String OPERATION_PATTERN_VERSION_GATEWAY = "versiongateway";
	
	
	public static final String DEFAULT_ROUTING_RULE = "/?.*";


	
	public static final String UPLOADED_FILES_KEY="uploadedFiles";


	public static final String REPOSITORY_TOKEN = "@repository@";
	public static final String SERVICE_NAME_TOKEN = "@service-name@";
	
	
	public static final String ENV_DEV="dev";
	public static final String ENV_DEVELOPER="developer";
	
	public static final String aditionalPart_1 = "	<meta-component>";
	public static final String aditionalPart_2 = "		<pattern-gateway-endpoint>http://@mpgw-host@:@mpgw-port@</pattern-gateway-endpoint>";
	public static final String aditionalPart_3 ="		<composition-gateway-endpoint>http://@mpgw-host@:@sg-mpgw-port@</composition-gateway-endpoint>";
	public static final String aditionalPart_4 =  "	</meta-component>";
}
