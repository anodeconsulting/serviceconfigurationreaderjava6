
package com.np.servicefactory.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.np.servicefactory.model.EnvironmentSpecificProperties;
import com.np.servicefactory.model.ServiceDetail;
import com.np.servicefactory.model.ServiceEnvironment;
import com.np.servicefactory.model.ServiceFactoryFolderStructure;
 
public class PropertiesConfigUtil {		
	public static void updatePropertiesConfig(Map<String, String> listOfProperties, String propertyFilePath){
		try{
		
			PropertiesConfiguration config = new PropertiesConfiguration(propertyFilePath);
			
			 Iterator<Entry<String, String>> it = listOfProperties.entrySet().iterator();
			    while (it.hasNext()) {
			    	@SuppressWarnings("rawtypes")
					Map.Entry pair = (Map.Entry)it.next();
			    	config.setProperty(pair.getKey().toString() , pair.getValue());
			    }
			
			    config.save();
		}catch (Exception e){
			e.printStackTrace();
		}
		
		System.out.println("Config Property Successfully Updated..");
	}
	
	public static void addProperties(Map<String, String> listOfProperties, String propertyFilePath) throws IOException {
		
		FileWriter fileWritter = new FileWriter(propertyFilePath, true);
		BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
		 Iterator<Entry<String, String>> it = listOfProperties.entrySet().iterator();
		    while (it.hasNext()) {
		    	@SuppressWarnings("rawtypes")
				Map.Entry pair = (Map.Entry)it.next();
		    	bufferWritter.append(pair.getKey().toString() + "=" + (String) pair.getValue());	
		    	bufferWritter.append(System.getProperty("line.separator"));
		    }
		
		bufferWritter.close();
	}
	
	public static String getPropertyFileName(String envName, boolean isForShared) {
		String lowercaseEnvName = StringUtils.lowerCase(envName);
		String firstPartOfFileName = null;
		if (isForShared) {
			firstPartOfFileName =  "build.shared";
		} else {
			firstPartOfFileName = "build";
		}
		
		String fileName = firstPartOfFileName + "." +lowercaseEnvName +".properties";
		return fileName;
	}
	
	public static void createPropertyFileFromProtoType( ServiceDetail serviceDetail, String protoTypePropertyFile, String protoTypeSharedPropertyFileName) throws IOException {
		ServiceFactoryFolderStructure folderStructure = serviceDetail.getServiceFolder();
		File protoType = new File(protoTypePropertyFile);
		String dir = folderStructure.getConfigDir();
		ServiceEnvironment serviceEnvironment = serviceDetail.getEnvironment();
	
		String targetPropertyFileName = null;
		File targetPropertyFile = null;
		File targetFile = null;
		
		List<EnvironmentSpecificProperties> environmentSpecificaProps = serviceEnvironment.getEnvironments();
		
		for(EnvironmentSpecificProperties environmentSpecificProperties : environmentSpecificaProps) {
			targetPropertyFileName = dir
					+ "/"
					+ PropertiesConfigUtil.getPropertyFileName(
							environmentSpecificProperties.getEnvironmentName(), false);
			targetPropertyFile = new File(targetPropertyFileName);

			FileUtils.copyFile(protoType, targetPropertyFile);

			String targetDevSharedPropFileName = dir
					+ "/"
					+ PropertiesConfigUtil.getPropertyFileName(
							environmentSpecificProperties.getEnvironmentName(), true);
			File sharedProtoType = new File(protoTypeSharedPropertyFileName);
			targetFile = new File(targetDevSharedPropFileName);
			FileUtils.copyFile(sharedProtoType, targetFile);
		}
	}
}

