//Import all needed packages
package com.np.servicefactory.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtils {
	public static void main(String[] args) {
		zipIt("C:/Users/bill/Documents/service_factory/upload_file/service_factory_temp//src.zip", "C:/Users/bill/Documents/service_factory/upload_file/service_factory_temp/src");
	}
	static public void zipIt(String zipFile, String sourceFolder) {
		byte[] buffer = new byte[1024];
		String source = "";
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		FileInputStream in = null;
		List<String> fileList = new ArrayList<String>();
		
		
		try {
			
			fos = new FileOutputStream(zipFile);
			zos = new ZipOutputStream(fos);
			generateFileList(new File(sourceFolder), zos, in, sourceFolder);
			
			System.out.println("Output to Zip : " + zipFile);
			

			
	
			zos.closeEntry();
			System.out.println("Folder successfully compressed");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if(in != null) {
				   in.close();
				}
				zos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	static private void generateFileList(File node, ZipOutputStream zos, FileInputStream in,  String sourceFolder) throws IOException {
		byte[] buffer = new byte[1024];
	
		
		// add file only
		if (node.isFile()) {
			String fileName = node.toString();
			String zipEntry = fileName.substring(sourceFolder.length() + 1,
					fileName.length());

			System.out.println("File Added : " + zipEntry);
			ZipEntry ze = new ZipEntry(getSource(sourceFolder) + File.separator
					+ zipEntry);
			zos.putNextEntry(ze);

			in = new FileInputStream(sourceFolder + File.separator + zipEntry);
			int len;
			while ((len = in.read(buffer)) > 0) {
				zos.write(buffer, 0, len);
			}
			
			in.close();

		}

		if (node.isDirectory()) {
			String[] subNote = node.list();
			if(subNote.length == 0) {
				   System.out.println(node.getPath() + " is empty");
			       File f = new File(node.getPath() + "/");
			       f.mkdir();       
			       String fileName = node.toString();
			       String zipEntry = fileName.substring(sourceFolder.length() + 1,
							fileName.length()) + "\\";
			   	System.out.println("File Added : " + zipEntry);
			       zos.putNextEntry(new ZipEntry(getSource(sourceFolder) + File.separator
							+ zipEntry));     
			     
			}
			
			for (String filename : subNote) {
				generateFileList(new File(node, filename),  zos, in,  sourceFolder);
			}
		}
	}
	
	private static String getSource(String sourceFolder) {
		String source = null;
		try {
			source = sourceFolder.substring(
					sourceFolder.lastIndexOf("/") + 1,
					sourceFolder.length());
		} catch (Exception e) {
			source = sourceFolder;
		}
		return source;
	}

	
}