
/**
 *  WSDL Util
 * 
 */

package com.np.servicefactory.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.namespace.QName;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;

import com.np.servicefactory.model.ServiceDetail;
import com.np.servicefactory.service.ServiceFacotryHelpService;
import com.predic8.schema.Import;
import com.predic8.schema.Include;
import com.predic8.schema.Schema;
import com.predic8.wsdl.Definitions;
import com.predic8.wsdl.Operation;
import com.predic8.wsdl.PortType;
import com.predic8.wsdl.Service;
import com.predic8.wsdl.WSDLParser;

public class WSDLOperationsUtil {
	private static final Logger logger = Logger.getLogger(WSDLOperationsUtil.class);
	
	public static void main(String[] args) {
		WSDLOperationsUtil parser = new WSDLOperationsUtil();

		String sourceWsdlFilePath = "C:/Workspace/Project/NextPathWay/IBIT/Developement1/ManageCustomerService/wsdl/ManageCustomerRate.wsdl";
		String targetWsdlFilePath = "C:/Workspace/Project/NextPathWay/IBIT/Developement1/ManageCustomerService/wsdl/ManageCustomerRate1.wsdl";
		
		String sourceXSDFilePath = "C:/Workspace/Project/NextPathWay/IBIT/Developement1/ManageCustomerService/xsd/ActivityLogInq_Msg.xsd";
		String targetXSDFilePath = "C:/Workspace/Project/NextPathWay/IBIT/Developement1/ManageCustomerService/xsd/ActivityLogInq_Msg1.xsd";

		
		//parser.replaceSchemaLoc(sourceWsdlFilePath,targetWsdlFilePath);
		
	//	parser.replaceSchemaLoc(sourceXSDFilePath,targetXSDFilePath);
		
	}

	/*
	 * Retrieve all the operation names based on incoming WSDL file path
	 */
	public static void getServiceInfoFromWSDL(String wsdlFilePath, ServiceDetail serviceDetail) {
		
		
		WSDLParser parser = new WSDLParser();
		List<String> operationList = new ArrayList<String>();
		Set<String> dependentXSDFiles = new HashSet<String>();
	
		Definitions defs = parser.parse(wsdlFilePath);
		
	

		for (PortType pt : defs.getPortTypes()) {
			System.out.println(pt.getName());
			for (Operation op : pt.getOperations()) {
				operationList.add(op.getName());
				System.out.println(" -" + op.getName());
			}
		}
		
		for (Schema schema : defs.getSchemas()) {
			   
			List<Import> schemaNames = schema.getImports();
			for(Import importSchema : schemaNames) {
				String schemaLocation = importSchema.getSchemaLocation();
				if(!StringUtils.isBlank(schemaLocation) ) {
					String[] parts = schemaLocation.split("/");
					String schemaFileName = parts[parts.length -1];
					dependentXSDFiles.add(schemaFileName);					
				}
			}
			
			List<Include> includeNames = schema.getIncludes();
			
			for(Include includeSchema : includeNames) {
				String schemaLocation = includeSchema.getSchemaLocation();
				if(!StringUtils.isBlank(schemaLocation) ) {
					String[] parts = schemaLocation.split("/");
					String schemaFileName = parts[parts.length -1];
					dependentXSDFiles.add(schemaFileName);					
				}
			}
		}
		
		
		String targetNamespace = defs.getTargetNamespace();
		System.out.println(" -" + targetNamespace);
		
		List<Service> services = defs.getServices();
		
		String serviceName = services.get(0).getName();
		String portName = services.get(0).getPorts().get(0).getName();	
		
		serviceDetail.setOperationNameList(operationList);
		serviceDetail.setPortName(portName);
		serviceDetail.setServiceName(serviceName);
		serviceDetail.setServiceTargetNameSpace(targetNamespace);	
		serviceDetail.setServiceDependentXSDFiles(dependentXSDFiles);
	}

	/*
	 * Verify if the WSDL is valid
	 */
	public static boolean isWSDLValid(String wsdlFilePath) {
		WSDLParser parser = new WSDLParser();
		boolean isWSDLValid = true;

		try {
			Definitions defs = parser.parse(wsdlFilePath);
		} catch (Exception e) {
			logger.error(e);	
			isWSDLValid = false;
		}

		System.out.println("wsdl " + isWSDLValid);
		return isWSDLValid;
	}

	/*
	 * replace Schema Location
	 */
	public static void replaceSchemaLoc(String sourceFilePath,String targetFilePath, boolean isWSDL) {
		System.out.println("Update location path in WSDL or XSD");
		System.out.println("sourceFilePath: " +  sourceFilePath);
		System.out.println("targetFilePath: " +  targetFilePath);
		
		try {
			String line = null;
			BufferedReader br = new BufferedReader(new FileReader(sourceFilePath));
			PrintWriter pr = new PrintWriter(new BufferedWriter(new FileWriter(targetFilePath,false)));

			while ((line = br.readLine()) != null) {
				String[] tempXSDStr=null;
				
				String lowercaseLine = line.toLowerCase();
				
				if (lowercaseLine.indexOf("schemalocation=") != -1) {
					String value = line.substring(lowercaseLine.indexOf(ServiceFatoryConstant.ServiceFactory_Source_WSDL_SchemaLocation_Key)+16, lowercaseLine.lastIndexOf(ServiceFatoryConstant.ServiceFactory_XSD_Type));
					tempXSDStr = value.trim().split("/");
					
					int total = tempXSDStr.length;
				    if(isWSDL) {
				    	line =line.replace(value, "../xsd/" + tempXSDStr[total-1]);
				    } else {
				    	line =line.replace(value, tempXSDStr[total-1]);
				    }
				}
				
				pr.println(line);
			}

			
			br.close();
			br = null;

			pr.close();
			pr = null;

		} catch (java.io.FileNotFoundException ex) {
			logger.error(ex);	
		} catch (java.io.IOException ex) {
			logger.error(ex);	
			ex.printStackTrace();
		}

	}
	
	public static  void removeNamespaces(XmlObject root) {
		String s;
		XmlCursor cursor = root.newCursor();
		cursor.toNextToken();
		while (cursor.hasNextToken()) {
			if (cursor.isNamespace()) {
				cursor.removeXml();
			} else {
				if (cursor.isStart() || cursor.isAttr()) {
					s = cursor.getName().getLocalPart();
					cursor.setName(new QName(s));
				}
				cursor.toNextToken();
			}
		}
		cursor.dispose();
	}

}