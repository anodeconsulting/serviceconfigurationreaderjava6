package com.np.servicefactory.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.np.servicefactory.service.ServiceFacotryHelpService;

@Controller
public class FileUploadController {
	static final String ROOT_DIR="C:\\Users/bill\\Documents\\service_factory\\upload_file\\";
	List<String> uploadedFileList = new ArrayList<String>();
	 @Autowired private ServiceFacotryHelpService serviceFacotryHelpService;

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
	public String displayForm() throws IOException {
    	FileUtils.cleanDirectory(new File(ROOT_DIR + "tmp/")); 
    	uploadedFileList.clear();
	     return "uploadfile";
	}
    
   
    
   
    @RequestMapping(value = "/uploadWSDLTest", method = RequestMethod.POST)
    public String uploadWSDLTest(
            @ModelAttribute("uploadForm") FileUpload uploadForm,
            Model map) throws Exception {
       
        List<MultipartFile> crunchifyFiles = uploadForm.getFiles();
 
        List<String> dependentFiles = new ArrayList<String>();
 
        if (null != crunchifyFiles && crunchifyFiles.size() > 0) {
            for (MultipartFile multipartFile : crunchifyFiles) {
 
                String fileName = multipartFile.getOriginalFilename();
                uploadedFileList.add(fileName);
                if (!"".equalsIgnoreCase(fileName)) {
                    // Handle file content - multipartFile.getInputStream()
                	File  uploadedFile = new File(ROOT_DIR + "tmp/"+fileName);             
                    multipartFile.transferTo(uploadedFile);
                    dependentFiles = this.processWSDL(uploadedFile, fileName);
            
                }
            }
        }
 
        map.addAttribute("depenentFiles", dependentFiles);
        uploadedFileList.addAll(dependentFiles);
        
        if(dependentFiles == null || dependentFiles.size() == 0) {
        	map.addAttribute("uploadedFiles", uploadedFileList);
        	return "uploadfilesuccess";
        }
        return "uploadOtherDocs";
    }
    
    @RequestMapping(value = "/uploadDependentFiles", method = RequestMethod.POST)
    public String uploadSchemaFiles(
            @ModelAttribute("uploadForm") FileUpload uploadForm,
            Model map) throws Exception {
       
        List<MultipartFile> crunchifyFiles = uploadForm.getFiles();
 
        List<String> allDependentFiles = new ArrayList<String>();
 
        if (null != crunchifyFiles && crunchifyFiles.size() > 0) {
            for (MultipartFile multipartFile : crunchifyFiles) {
 
                String fileName = multipartFile.getOriginalFilename();
                if (!"".equalsIgnoreCase(fileName)) {
                	File  uploadedFile = new File(ROOT_DIR + "tmp/"+fileName);             
                    multipartFile.transferTo(uploadedFile);
                    List<String> dependentFiles = this.processUploadedFile(uploadedFile, fileName);
                    allDependentFiles.addAll(dependentFiles);
                }
            }
        }
 
        map.addAttribute("depenentFiles", allDependentFiles);
        uploadedFileList.addAll(allDependentFiles);
      
        if(allDependentFiles == null || allDependentFiles.size() == 0) {
        	map.addAttribute("uploadedFiles", uploadedFileList);
        	return "uploadfilesuccess";
        }
        return "uploadOtherDocs";
    }
    
    private List<String> processUploadedFile(File file, String fileName) throws Exception {
    	List<String> dependentFiles = new ArrayList<String>();
    	if(isWSDLFile(fileName)) {
    		dependentFiles.addAll(this.processWSDL(file, fileName));
    	} else {
    		dependentFiles.addAll(processSchema(file, fileName));
    	}
    	
    	return dependentFiles;
    }
    
    private boolean isWSDLFile(String fileName) {
    	String [] strArray = fileName.split("\\.");
    	
    	String fileSuffix= strArray[(strArray.length -1)];
    	if("WSDL".equalsIgnoreCase(fileSuffix)) {
    		return true;
    	}
    	return false;
    }
    
    private List<String> processWSDL(File file, String fileName) throws Exception {
    	  DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
          
          factory.setNamespaceAware(true);
          DocumentBuilder builder;
          Document doc = null;
          
          builder = factory.newDocumentBuilder();
          doc = builder.parse(file);
          XPathFactory xpathFactory = XPathFactory.newInstance();

          // Create XPath object
          XPath xpath = xpathFactory.newXPath();
          List<String> dependentFiles = new ArrayList<String>();
          XPathExpression expr =
                  xpath.compile("//*[local-name()='schema']/*[local-name()='import']/@schemaLocation");
              //evaluate expression result on XML document
          NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
          for (int i = 0; i < nodes.getLength(); i++) {
        	  dependentFiles.add(nodes.item(i).getNodeValue());
        	    String schemafileName = this.getFileName(nodes.item(i).getNodeValue());
        	    
              	nodes.item(i).setNodeValue("../../xsd/"+ schemafileName);
               
          }
          
          expr = xpath.compile("//*[local-name()='import']/@location");
           
          nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
           for (int i = 0; i < nodes.getLength(); i++) {
         	  dependentFiles.add(nodes.item(i).getNodeValue());
         	    String wsdlName = this.getFileName(nodes.item(i).getNodeValue());
         	    
               	nodes.item(i).setNodeValue(wsdlName);
                
           }
          
          Transformer xformer = TransformerFactory.newInstance().newTransformer();
          xformer.transform(new DOMSource(doc), new StreamResult(new File(ROOT_DIR +"wsdl/" + fileName)));
          return dependentFiles;
    }
    
    
    private List<String> processSchema(File file, String fileName) throws Exception {
  	  DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        
        factory.setNamespaceAware(true);
        DocumentBuilder builder;
        Document doc = null;
        
        builder = factory.newDocumentBuilder();
        doc = builder.parse(file);
        XPathFactory xpathFactory = XPathFactory.newInstance();

        // Create XPath object
        XPath xpath = xpathFactory.newXPath();
        List<String> dependentFiles = new ArrayList<String>();
        XPathExpression expr =
                xpath.compile("//*[local-name()='schema']/*[local-name()='import']/@schemaLocation");
            //evaluate expression result on XML document
        NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++) {
      	  dependentFiles.add(nodes.item(i).getNodeValue());
      	    String schemafileName = this.getFileName(nodes.item(i).getNodeValue());
      	    
            	nodes.item(i).setNodeValue("../../xsd/"+ schemafileName);
             
        }
        
        Transformer xformer = TransformerFactory.newInstance().newTransformer();
        xformer.transform(new DOMSource(doc), new StreamResult(new File(ROOT_DIR +"xsd/" + fileName)));
        return dependentFiles;
  }
    private String getFileName(String schemaLocation){
    	String [] strArray = schemaLocation.split("/");
		
		String fileName = strArray[(strArray.length -1)];
		return fileName;
    }
}
