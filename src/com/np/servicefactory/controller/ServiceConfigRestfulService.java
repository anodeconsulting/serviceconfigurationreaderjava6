package com.np.servicefactory.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.np.servicefactory.model.CommonRestfulServiceRes;
import com.np.servicefactory.model.OperationDetail;
import com.np.servicefactory.model.ProcessUploadWSDLRes;
import com.np.servicefactory.model.ProcessWsdlRequest;
import com.np.servicefactory.model.RetrieveServiceDetailRes;
import com.np.servicefactory.model.ServiceConfig;
import com.np.servicefactory.model.ServiceDetail;
import com.np.servicefactory.model.ServiceEnvironment;
import com.np.servicefactory.model.ServiceFactoryFolderStructure;
import com.np.servicefactory.model.ServiceProvider;
import com.np.servicefactory.model.UploadedFiles;
import com.np.servicefactory.service.ManifestEntryManagement;
import com.np.servicefactory.service.ResponderConfigManagement;
import com.np.servicefactory.service.ServiceConfigManagement;
import com.np.servicefactory.service.ServiceFacotryHelpService;
import com.np.servicefactory.service.ServiceFactoryPropertyService;
import com.np.servicefactory.util.ServiceFatoryConstant;
import com.np.servicefactory.util.WSDLOperationsUtil;

@RestController
public class ServiceConfigRestfulService {
	@Autowired private ServiceFacotryHelpService serviceFacotryHelpService;
	@Autowired  private ServiceConfigManagement serviceConfigManagement;
	@Autowired  private ResponderConfigManagement  responderConfigManagement;
	@Autowired  private ManifestEntryManagement manifestEntryManagement;
	@Autowired  private ServiceFactoryPropertyService serviceFactoryPropertyService;
	@Autowired 	private Environment env;
	
	@RequestMapping(value = "/processUploadWsdl", method = RequestMethod.POST, consumes = "application/json", produces =   "application/json")
	public ProcessUploadWSDLRes processUploadWsdl(@RequestBody ProcessWsdlRequest pwr, HttpSession session) {
		String rootWsdlName = pwr.getRootWsdlName();
		ServiceDetail serviceDetail = (ServiceDetail) session.getAttribute("serviceDetail");
		serviceDetail.setRootWsdlName(rootWsdlName);
		
		String rootWsdlDir = serviceDetail.getServiceFolder().getWsdlDir();
		

		
		List<String> operationNameList = new ArrayList<String>();
		
		boolean isWsdlValid = WSDLOperationsUtil.isWSDLValid(rootWsdlDir + "/"+rootWsdlName);
		ProcessUploadWSDLRes res = new ProcessUploadWSDLRes();
		if(!isWsdlValid) {	
			
		    res.setStatus("false");
			res.setOperationNameList(operationNameList);
		} else {
			WSDLOperationsUtil.getServiceInfoFromWSDL(rootWsdlDir + "/" + rootWsdlName, serviceDetail);
			res.setOperationNameList(serviceDetail.getOperationNameList());
			
			
		}
		return res;
	}
	
	@RequestMapping(value = "/processServiceInfo", method = RequestMethod.POST, consumes = "application/json", produces =   "application/json")
	public CommonRestfulServiceRes processServiceInfo(@RequestBody ServiceConfig serviceConfig, HttpSession session) {
		
			
		ServiceDetail serviceDetail = (ServiceDetail) session.getAttribute("serviceDetail");
		serviceDetail.setOperationDetails(serviceConfig.getOperationDetails());
		serviceDetail.setFrontSideHandlers(serviceConfig.getFrontSideHandlers());   
		serviceDetail.setBusinessDomain(serviceConfig.getBusinessDomain());
		serviceFacotryHelpService.copySGConfigFileToXmlFolder(serviceDetail);
	    
		CommonRestfulServiceRes res = new CommonRestfulServiceRes();
		return res;
	}
	
	@RequestMapping(value = "/processEnvironmentProperties", method = RequestMethod.POST, consumes = "application/json", produces =   "application/json")
	public CommonRestfulServiceRes processEnvironmentProperties(@RequestBody ServiceEnvironment serviceEnvironment, HttpSession session) {
			
		ServiceDetail serviceDetail = (ServiceDetail) session
				.getAttribute("serviceDetail");
		CommonRestfulServiceRes res = new CommonRestfulServiceRes();
		UploadedFiles uploadedFiles = (UploadedFiles)session.getAttribute(ServiceFatoryConstant.UPLOADED_FILES_KEY);
		serviceFacotryHelpService.cleanupUploadedfiles(serviceDetail, uploadedFiles);
		
		serviceFacotryHelpService.resetWSDAndXSDFileList(serviceDetail);
		
		
		serviceDetail.setEnvironment(serviceEnvironment);
		try {
			serviceConfigManagement.createServiceConfigFile(serviceDetail);
						
			
			serviceConfigManagement.addAdditionalPart(serviceDetail);
			responderConfigManagement.createResponseConfigFile(serviceDetail);
			
			ServiceFactoryFolderStructure serviceStructure = serviceDetail.getServiceFolder();
			
			String logFilePath = serviceStructure.getXmlDir() + "/LogConfig.xml";
			ServiceFacotryHelpService.processLogFileAndDataValidationFile(logFilePath, serviceDetail);
			
			String dataValidationFilePath = serviceStructure.getXmlDir() + "/DataValidatorConfig.xml";
			ServiceFacotryHelpService.processLogFileAndDataValidationFile(dataValidationFilePath, serviceDetail);
			
			manifestEntryManagement.createDeploymentManifest(serviceDetail, uploadedFiles);

			manifestEntryManagement.createDeploymentManifestSerivceUT(serviceDetail, uploadedFiles);
			manifestEntryManagement.createDeploymentManifestUT(serviceDetail);			
			serviceFactoryPropertyService.createPropertyFileFromProtoType(serviceDetail);
			serviceFactoryPropertyService.generatePropertyFile(serviceDetail);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 
		return res;
	}
	
	
	@RequestMapping(value = "/retrieveOperationDetails", method = RequestMethod.GET, produces =   "application/json")
	public RetrieveServiceDetailRes retrieveServiceDetail(HttpSession session) {
		
		
		ServiceDetail serviceDetail = (ServiceDetail) session
				.getAttribute("serviceDetail");
        /*ServiceDetail serviceDetail = new ServiceDetail();
	    List<OperationDetail> operationDetails = new ArrayList<OperationDetail>();
	    OperationDetail operationDetail = new OperationDetail();
	    operationDetail.setOperationName("test");
	    
	    List<ServiceProvider> serviceProviders = new ArrayList<ServiceProvider>();
	    ServiceProvider serviceProvider = new ServiceProvider();
	   
	    serviceProvider.setInputTransformation("");
	    serviceProvider.setOutputTransformation("");
	    serviceProvider.setBackEndUrlKey("backEndKey");
	    serviceProvider.setRoutingRule("rultingRulerultingRulerultingRulerulti"
	    		+ "ngRulerultingRulerultingRulerultingRulerultingRuledddddddddd");
	    serviceProviders.add(serviceProvider);	
	    operationDetail.setPattern("odbc");
	    operationDetail.setProviders(serviceProviders);
	    operationDetails.add(operationDetail);
	       
	    serviceProvider.setSqlDatasourceNameKey("sql_datasource_name_1");
	    serviceProvider.setSqlStatementKey("sql_statement_key_1");
	    
	    OperationDetail operationDetail2 = new OperationDetail();
	    operationDetail2.setOperationName("test2");
	    operationDetail2.setPattern("odbc");
	    List<ServiceProvider> serviceProviders2 = new ArrayList<ServiceProvider>();
	    ServiceProvider serviceProvider2 = new ServiceProvider();
	 
	    serviceProvider2.setBackEndUrlKey("backEndKey");
	    serviceProvider2.setRoutingRule("rultingRulerultingRulerultingRulerulti"
	    		+ "ngRulerultingRulerultingRulerultingRulerultingRuledddddddddd");
	    serviceProviders2.add(serviceProvider2);
	    serviceProvider2.setSqlDatasourceNameKey("sql_datasource_name_2");
	    serviceProvider2.setSqlStatementKey("sql_statement_key_2");
	    operationDetail2.setProviders(serviceProviders2);
	    operationDetails.add(operationDetail2);
	   	    
	    serviceDetail.setOperationDetails(operationDetails);*/
		
	    String environmentsString = env.getProperty("environments");
		String[] environments = environmentsString.split(",");
		List<String> environmentList = Arrays.asList(environments);
		
		RetrieveServiceDetailRes res = new RetrieveServiceDetailRes();
		
		
		res.setOperationDetails(serviceDetail.getOperationDetails());
		res.setEnvironments(environmentList);
		
		return res;
	}
	
	
	
}
