package com.np.servicefactory.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.np.servicefactory.model.ServiceDetail;
import com.np.servicefactory.model.ServiceFactoryFolderStructure;
import com.np.servicefactory.service.ServiceFacotryHelpService;
import com.np.servicefactory.util.ZipUtils;

@Controller
public class DownloadController {
	 public static String OUTPUT_FILE_NAME = "src.zip";
	 private static final int BUFFER_SIZE = 4096;
	 
	 @Autowired private ServiceFacotryHelpService serviceFacotryHelpService;
	 @RequestMapping(value = "/download", method = RequestMethod.GET)
		public String displayForm() throws IOException {   	
		     //serviceFacotryHelpService.createDirStructrue();
		     return "download";
		}
	  
	  @RequestMapping(value = "/downloadFile", method = RequestMethod.GET)
	    public void downloadFile(HttpServletRequest request,
	            HttpServletResponse response,  HttpSession session ) throws IOException {	 
	    
	        ServiceDetail serviceDetail = (ServiceDetail) session.getAttribute("serviceDetail");
	        ServiceFactoryFolderStructure serviceFolder = serviceDetail.getServiceFolder();
	        
	        
	        String workDir = serviceFolder.getWorkingDir();
	        String srcFolder = serviceFolder.getSrcDir();
	        
	        String srcFolerWithoutLastForwadSlash = srcFolder.substring(0, srcFolder.length() -1);
	        
	        String filePath = workDir + "/"+ OUTPUT_FILE_NAME;
	        
	        ZipUtils.zipIt(filePath, srcFolerWithoutLastForwadSlash);
	        
	        
	        File downloadFile = new File(filePath);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	        ServletContext context = request.getServletContext();
	        
	        String mimeType = context.getMimeType(filePath);
	        if (mimeType == null) {
	            // set to binary type if MIME mapping not found
	            mimeType = "application/octet-stream";
	        }
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	        
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	 
	        // get output stream of the response
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	 
	        // write bytes read from the input stream into the output stream
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();
	    }
}
