package com.np.servicefactory.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.np.servicefactory.model.ServiceDetail;
import com.np.servicefactory.model.ServiceFactoryFolderStructure;
import com.np.servicefactory.model.UploadedFiles;
import com.np.servicefactory.service.ServiceFacotryHelpService;
import com.np.servicefactory.util.ServiceFatoryConstant;
import com.np.servicefactory.util.UnzipUtil;

@Controller
public class ServiceConfigController {
	 
     @Autowired private ServiceFacotryHelpService serviceFacotryHelpService;
 	
 	 
	 @RequestMapping(value = "/serviceConfig", method = RequestMethod.GET)
	 public String displayServiceConfig(HttpSession session) { 
		 System.out.println("*****************************");
		  System.out.println("Sungang's test.");
		  System.out.println("*****************************");
		 //Log testlog = new Log();
		  //testlog.info();
		  ServiceFactoryFolderStructure serviceFolder = serviceFacotryHelpService.createDirStructrue();	
		  
		  UploadedFiles uploadedFiles = new UploadedFiles();
		  session.setAttribute(ServiceFatoryConstant.UPLOADED_FILES_KEY, uploadedFiles);
		  
		  ServiceDetail serviceDetail = new ServiceDetail();
		  serviceDetail.setServiceFolder(serviceFolder);
		  session.setAttribute("serviceDetail", serviceDetail);
		 
		  return "service_config";
		}

	  @RequestMapping(value = "/uploadReqDPA", method = RequestMethod.POST)
	  public @ResponseBody String uploadReqDPA(
			  MultipartHttpServletRequest request, HttpServletResponse response, HttpSession session) {
		  ServiceDetail serviceDetail = (ServiceDetail) session.getAttribute("serviceDetail");
		  String folderPath = serviceDetail.getServiceFolder().getDpaDir();
		  String fileName = saveFileToFolder(request, folderPath);
		  String resString  = fileName +" uploaded! ";	
		 
		  UploadedFiles uploadedFiles = (UploadedFiles)session.getAttribute(ServiceFatoryConstant.UPLOADED_FILES_KEY);
		  uploadedFiles.addFileToDPASet(fileName);
		  
	      return resString;
	    }
	  
	  @RequestMapping(value = "/uploadReqXSLTFile", method = RequestMethod.POST)
	  public @ResponseBody String uploadReqXSLTFile(
			  MultipartHttpServletRequest request, HttpServletResponse response, HttpSession session) {
		  ServiceDetail serviceDetail = (ServiceDetail) session.getAttribute("serviceDetail");
		  String folderPath = serviceDetail.getServiceFolder().getXsltDir();
		  String fileName = saveFileToFolder(request, folderPath);
		  String resString  = fileName +" uploaded! ";	
		 
		  UploadedFiles uploadedFiles = (UploadedFiles)session.getAttribute(ServiceFatoryConstant.UPLOADED_FILES_KEY);
		  uploadedFiles.addFileToXSLTSet(fileName);
		  
	      return resString;
	    }
	  
	  @RequestMapping(value = "/uploadResXSLTFile", method = RequestMethod.POST)
	  public @ResponseBody String uploadResXSLTFile(
			  MultipartHttpServletRequest request, HttpServletResponse response, HttpSession session) {
		  ServiceDetail serviceDetail = (ServiceDetail) session.getAttribute("serviceDetail");
		  String folderPath = serviceDetail.getServiceFolder().getXsltDir();
		  String fileName = saveFileToFolder(request, folderPath);
		  String resString  = fileName +" uploaded! ";	
		 
		  UploadedFiles uploadedFiles = (UploadedFiles)session.getAttribute(ServiceFatoryConstant.UPLOADED_FILES_KEY);
		  uploadedFiles.addFileToXSLTSet(fileName);
		  
	      return resString;
	    }
	  
	  @RequestMapping(value = "/uploadSQLInputArgumentTransFile", method = RequestMethod.POST)
	  public @ResponseBody String uploadSQLInputArgumentTransFile(
			  MultipartHttpServletRequest request, HttpServletResponse response, HttpSession session) {		  
		  ServiceDetail serviceDetail = (ServiceDetail) session.getAttribute("serviceDetail");
		  String folderPath = serviceDetail.getServiceFolder().getXsltDir();
	      System.out.println(folderPath);
	      String fileName = saveFileToFolder(request, folderPath);
	      UploadedFiles uploadedFiles = (UploadedFiles)session.getAttribute(ServiceFatoryConstant.UPLOADED_FILES_KEY);
		  uploadedFiles.addFileToXSLTSet(fileName);
	      String resString  = fileName +" uploaded! ";		    
          return resString;
	    }
	  
	  @RequestMapping(value = "/uploadSQLResultTransformFile", method = RequestMethod.POST)
	  public @ResponseBody String uploadSQLResultTransformFile(
			  MultipartHttpServletRequest request, HttpServletResponse response, HttpSession session) {
		  
		  ServiceDetail serviceDetail = (ServiceDetail) session.getAttribute("serviceDetail");
		  String folderPath = serviceDetail.getServiceFolder().getXsltDir();
		  String fileName = saveFileToFolder(request, folderPath);
		  UploadedFiles uploadedFiles = (UploadedFiles)session.getAttribute(ServiceFatoryConstant.UPLOADED_FILES_KEY);
		  uploadedFiles.addFileToXSLTSet(fileName);
		  String resString  = fileName +" uploaded! ";		    
	      return resString;
	    }
	  
	  @RequestMapping(value = "/uploadResDPA", method = RequestMethod.POST)
	  public @ResponseBody String uploadResDPA(
			  MultipartHttpServletRequest request, HttpServletResponse response, HttpSession session) {	
		  ServiceDetail serviceDetail = (ServiceDetail) session.getAttribute("serviceDetail");
		  String folderPath = serviceDetail.getServiceFolder().getDpaDir();
		  String fileName = saveFileToFolder(request, folderPath);
		  String resString  = fileName +" uploaded! ";	
		  UploadedFiles uploadedFiles = (UploadedFiles)session.getAttribute(ServiceFatoryConstant.UPLOADED_FILES_KEY);
		  uploadedFiles.addFileToDPASet(fileName);
	      return resString;
	    }
	  	 
	  
	  @RequestMapping(value = "/uploadSampleReqFile", method = RequestMethod.POST)
	  public @ResponseBody String uploadSampleReqFile(
			  MultipartHttpServletRequest request, HttpServletResponse response, HttpSession session) {
		     ServiceDetail serviceDetail = (ServiceDetail) session.getAttribute("serviceDetail");
		  
		     String folderPath = serviceDetail.getServiceFolder().getTestReqDir();
		     String fileName = saveFileToFolder(request, folderPath);
		     
		     UploadedFiles uploadedFiles = (UploadedFiles)session.getAttribute(ServiceFatoryConstant.UPLOADED_FILES_KEY);
			 uploadedFiles.addFileToTestReqSet(fileName);
		     
		     
	        return "success";
	   }
	  
	@RequestMapping(value = "/uploadTestResFile", method = RequestMethod.POST)
	public @ResponseBody
	String uploadTestResFile(MultipartHttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ServiceDetail serviceDetail = (ServiceDetail) session
				.getAttribute("serviceDetail");
		String folderPath = serviceDetail.getServiceFolder().getTestResDir();
		String fileName = saveFileToFolder(request, folderPath);
		
		UploadedFiles uploadedFiles = (UploadedFiles) session
				.getAttribute(ServiceFatoryConstant.UPLOADED_FILES_KEY);
		uploadedFiles.addFileToTestResSet(fileName);
		return "success";
	}
	  @RequestMapping(value = "/uploadWSDL", method = RequestMethod.POST)
	  public @ResponseBody List<String> uploadWSDLFolder(
			  MultipartHttpServletRequest request, HttpServletResponse response, HttpSession session) {
		  ServiceDetail serviceDetail = (ServiceDetail) session.getAttribute("serviceDetail");
		  String folderPath = serviceDetail.getServiceFolder().getWorkingDir();
		  String fileName = saveFileToFolder(request, folderPath);
		  String tmpPath = serviceDetail.getServiceFolder().getTmpDir();
		  UnzipUtil.unZipIt(folderPath + "/" + fileName, tmpPath);
		  
		  serviceFacotryHelpService.processWSDLAndXSDfiles(serviceDetail.getServiceFolder(), serviceDetail);
	      return serviceDetail.getWsdlFiles();
	    }
	  
	  private String saveFileToFolder(MultipartHttpServletRequest request, String folderPath) {
		  Iterator<String> itr =  request.getFileNames();
	         MultipartFile multipartFile = null;
	      
	         String fileName = null;
	         while(itr.hasNext()){        	 

	        	 multipartFile = request.getFile(itr.next()); 
	            fileName = multipartFile.getOriginalFilename();
	             System.out.println(fileName +" uploaded! ");
	             try {
	            	 File  uploadedFile = new File( folderPath + "/"+fileName);             
	                 multipartFile.transferTo(uploadedFile);
	 
	            } catch (IOException e) {	                
	                e.printStackTrace();
	            }	           
	         }
	        return fileName;		  
	  }
}
