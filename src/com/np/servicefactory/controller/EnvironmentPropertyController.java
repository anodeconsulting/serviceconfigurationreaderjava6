package com.np.servicefactory.controller;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.np.servicefactory.service.ServiceFacotryHelpService;

@Controller
public class EnvironmentPropertyController {
	@Autowired
	Environment env;
	
	@Autowired
	private ServiceFacotryHelpService serviceFacotryHelpService;
	private static final Logger logger = Logger.getLogger(EnvironmentPropertyController.class);
	final static int BUFFER_SIZE = 2180;

	@RequestMapping(value = "/environmentProperties", method = RequestMethod.GET)
	public String displayEnvironmentPropertyPage() throws IOException {
		String environmentsString = env.getProperty("environments");
		String[] environments = environmentsString.split(",");
		
		logger.info("start environment page");
		return "environment_properties";
	}
	
	
}
