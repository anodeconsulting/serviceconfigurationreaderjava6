package com.np.servicefactory.service;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.xmlbeans.XmlOptions;
import org.springframework.stereotype.Service;

import com.bns.ibit.servicefactory.template.manifest.v1.ManifestDocument;
import com.bns.ibit.servicefactory.template.manifest.v1.ManifestDocument.Manifest;
import com.bns.ibit.servicefactory.template.manifest.v1.MigrateFileSystemObjectDocument.MigrateFileSystemObject;
import com.bns.ibit.servicefactory.template.manifest.v1.MigrateFileSystemObjectsDocument.MigrateFileSystemObjects;
import com.bns.ibit.servicefactory.template.manifest.v1.SourceFolderType;
import com.bns.ibit.servicefactory.template.manifest.v1.TargetFolderType;
import com.np.servicefactory.model.OperationDetail;
import com.np.servicefactory.model.ServiceDetail;
import com.np.servicefactory.model.ServiceFactoryFolderStructure;
import com.np.servicefactory.model.ServiceProvider;
import com.np.servicefactory.model.TestConfiguration;
import com.np.servicefactory.model.UploadedFiles;
import com.np.servicefactory.util.ServiceFatoryConstant;
import com.np.servicefactory.util.WSDLOperationsUtil;


@Service("manifestEntryManagement")
public class ManifestEntryManagement {
	
	public static void main(String[] args) {
		ServiceDetail serviceDetail = new ServiceDetail();
		
		
		ManifestEntryManagement manifestEntry = new ManifestEntryManagement();
		
		List<String> wsdlfiles = new ArrayList<String>();
		
		wsdlfiles.add("ManageCustomerRate.wsdl");
		
		List<String> xsdfiles = new ArrayList<String>();
		
		xsdfiles.add("GetExchangeRateRequest.xsd");
		xsdfiles.add("GetExchangeRateResponse.xsd");
		xsdfiles.add("ManageCustomer_REQUEST.xsd");
		xsdfiles.add("ManageCustomer_RESPONSE.xsd");
		serviceDetail.setWsdlFiles(wsdlfiles);
		serviceDetail.setXsdFiles(xsdfiles);
		List<OperationDetail> operationDetails = new ArrayList<OperationDetail>();		

		OperationDetail operationDetail = new OperationDetail();
		operationDetail.setPattern("binary");
		List<ServiceProvider> providers = new ArrayList<ServiceProvider>();
		ServiceProvider provider = new ServiceProvider();
		provider.setInputTransformation("testReqdpa.txt");
		provider.setOutputTransformation("testResdap.txt");
		providers.add(provider);
		operationDetail.setProviders(providers);
		
		operationDetails.add(operationDetail);
		
		List<TestConfiguration> testConfigs = new ArrayList<TestConfiguration>();
		
		TestConfiguration testConfig = new TestConfiguration();
		testConfig.setResponseFileName("testResFile.txt");
		testConfig.setSampleSoapRequestFileName("sampleSOARequestFile.txt");
		testConfigs.add(testConfig);
		
		operationDetail.setTestConfigs(testConfigs);
	
		serviceDetail.setOperationDetails(operationDetails);
		try {
			//manifestEntry.createDeploymentManifest(serviceDetail);
			//manifestEntry.createDeploymentManifestSerivceUT(serviceDetail);
			manifestEntry.createDeploymentManifestUT(serviceDetail);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createDeploymentManifestSerivceUT(ServiceDetail serviceDetail, UploadedFiles uploadedFiles) throws IOException {
		ManifestDocument manifestDoc = null;
		manifestDoc = ManifestDocument.Factory.newInstance();
		Manifest manifest = manifestDoc.addNewManifest();
		MigrateFileSystemObjects migrateFileSystemObjects = manifest.addNewMigrateFileSystemObjects();
		manifest.setDomain("@ut-domain@");
		manifest.setEnv("@env@");
		MigrateFileSystemObject migrateFileSystemObject1 = migrateFileSystemObjects.addNewMigrateFileSystemObject();
		migrateFileSystemObject1.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_XML);
		migrateFileSystemObject1.setName(ServiceFatoryConstant.ServiceFactory_ResponderConfig_File);
		migrateFileSystemObject1.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_XML);
		
		Set<String> testResFiles = uploadedFiles.getTestResFiles();
		for (String fileName : testResFiles) {
			MigrateFileSystemObject migrateFileSystemObject3 = migrateFileSystemObjects.addNewMigrateFileSystemObject();
			migrateFileSystemObject3.setSourceFolder(SourceFolderType.SRC_TEST_DIR);
			migrateFileSystemObject3.setName(fileName);
			migrateFileSystemObject3.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_TEST);
		}
		
		
		 ServiceFactoryFolderStructure serviceFolder = serviceDetail.getServiceFolder();
		 File outputFile = new File(serviceFolder.getManitestDir() + "/" +   ServiceFatoryConstant.SF_DEPLOYMENT_MANIFEST_SERVICE_UT_File );
		 XmlOptions xmlOptions = new XmlOptions();
	     xmlOptions.setSavePrettyPrint();
	     
	     WSDLOperationsUtil.removeNamespaces(manifestDoc);
		 manifestDoc.save(outputFile, xmlOptions);
		 System.out.println("out===" + manifestDoc.toString());
		
	}
	
	public void createDeploymentManifest(ServiceDetail serviceDetail, UploadedFiles uploadedFiles) throws IOException {
		List<OperationDetail> operationDetails =serviceDetail.getOperationDetails();
		boolean isCompositeService = false;
		
		for(OperationDetail operationDetail : operationDetails) {
			if("true".equals(operationDetail.getCompositeServiceFlag())) {
				isCompositeService = true;
				break;
			}
		}
		
		ManifestDocument manifestDoc = ManifestDocument.Factory.newInstance();		
		Manifest manifest = manifestDoc.addNewManifest();
		manifest.setDomain(serviceDetail.getEnvironment().getDomainName());
		manifest.setEnv(serviceDetail.getEnvironment().getEnvironmentName());
		
		MigrateFileSystemObjects migrateFileSystemObjects = manifest.addNewMigrateFileSystemObjects();
		MigrateFileSystemObject migrateFileSystemObject1 = migrateFileSystemObjects.addNewMigrateFileSystemObject();
				
		migrateFileSystemObject1.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject1.setName(ServiceFatoryConstant.INFRA_EXTRACT_PAYLOAD_XSLT);
		migrateFileSystemObject1.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject2 = migrateFileSystemObjects.addNewMigrateFileSystemObject();
		migrateFileSystemObject2.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject2.setName(ServiceFatoryConstant.INFRA_FORMAT_ERROR_LOG_MSG_XSLT);
		migrateFileSystemObject2.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject3 = migrateFileSystemObjects.addNewMigrateFileSystemObject();		
		migrateFileSystemObject3.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject3.setName(ServiceFatoryConstant.INFRA_FORMAT_LOG_MSG_XSLT);
		migrateFileSystemObject3.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject4 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject4.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject4.setName(ServiceFatoryConstant.INFRA_GENERIC_SOAP_FAULT_XSLT);
		migrateFileSystemObject4.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject4_1 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject4_1.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject4_1.setName(ServiceFatoryConstant.INFRA_GENERIC_SOAP_FAULT_BIN_XSLT);
		migrateFileSystemObject4_1.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject5 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject5.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject5.setName(ServiceFatoryConstant.INFRA_RETRIEVE_SOAP_ACTION_VAR_XSLT);
		migrateFileSystemObject5.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject7 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject7.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject7.setName(ServiceFatoryConstant.INFRA_WRAP_PAYLOAD_XSLT);
		migrateFileSystemObject7.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject44 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject44.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject44.setName(ServiceFatoryConstant.INFRA_IDENTITY_XSLT);
		migrateFileSystemObject44.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);	
	
		
		MigrateFileSystemObject migrateFileSystemObject6 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject6.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject6.setName(ServiceFatoryConstant.INFRA_SET_SOAP_ACTION_VAR_XSLT);
		migrateFileSystemObject6.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		
		
		MigrateFileSystemObject migrateFileSystemObject29 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject29.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject29.setName(ServiceFatoryConstant.INFRA_AAA_POLICY_SWITCHER_XSLT);
		migrateFileSystemObject29.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject7_1 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject7_1.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject7_1.setName(ServiceFatoryConstant.INFRA_CUSTOM_ERROR_HANDLER_XSLT);
		migrateFileSystemObject7_1.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject7_2 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject7_2.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject7_2.setName(ServiceFatoryConstant.INFRA_SF_UTILITY_XSLT);
		migrateFileSystemObject7_2.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject7_3 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject7_3.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject7_3.setName(ServiceFatoryConstant.INFRA_EXTRACT_IDENTITY_XSLT);
		migrateFileSystemObject7_3.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject7_4 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject7_4.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject7_4.setName(ServiceFatoryConstant.INFRA_VALIDATE_CONSUMER_DATA_XSLT);
		migrateFileSystemObject7_4.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject7_5 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject7_5.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject7_5.setName(ServiceFatoryConstant.INFRA_CONVERT_BIN_XSLT);
		migrateFileSystemObject7_5.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject7_6 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject7_6.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject7_6.setName(ServiceFatoryConstant.INFRA_EXE_PCI_COMPLIANCE_XSLT);
		migrateFileSystemObject7_6.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject7_7 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject7_7.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
		migrateFileSystemObject7_7.setName(ServiceFatoryConstant.INFRA_ASYNC_LOG_SENDER_XSLT);
		migrateFileSystemObject7_7.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		
		if(isCompositeService) {
			MigrateFileSystemObject migrateFileSystemObject7_7_1 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject7_7_1.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
			migrateFileSystemObject7_7_1.setName(ServiceFatoryConstant.INFRA_SG_BUILD_SCATTER_URLS_XSLT);
			migrateFileSystemObject7_7_1.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
			
			MigrateFileSystemObject migrateFileSystemObject7_7_2 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject7_7_2.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
			migrateFileSystemObject7_7_2.setName(ServiceFatoryConstant.INFRA_SG_CONDITION_BUILDER_XSLT);
			migrateFileSystemObject7_7_2.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
			
			MigrateFileSystemObject migrateFileSystemObject7_7_3 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject7_7_3.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
			migrateFileSystemObject7_7_3.setName(ServiceFatoryConstant.INFRA_SG_CONTEXT_SWITCHER_BATCH_XSLT);
			migrateFileSystemObject7_7_3.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
			
			MigrateFileSystemObject migrateFileSystemObject7_7_4 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject7_7_4.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
			migrateFileSystemObject7_7_4.setName(ServiceFatoryConstant.INFRA_SG_CONTEXT_SWITCHER_SERIAL_XSLT);
			migrateFileSystemObject7_7_4.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
			
			MigrateFileSystemObject migrateFileSystemObject7_7_5 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject7_7_5.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
			migrateFileSystemObject7_7_5.setName(ServiceFatoryConstant.INFRA_SG_POSTACTION_EVALUATOR_XSLT);
			migrateFileSystemObject7_7_5.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
			
			MigrateFileSystemObject migrateFileSystemObject7_7_6 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject7_7_6.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
			migrateFileSystemObject7_7_6.setName(ServiceFatoryConstant.INFRA_SG_SERVICE_CALLER_POSTACTION_XSLT);
			migrateFileSystemObject7_7_6.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
			
			MigrateFileSystemObject migrateFileSystemObject7_7_7 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject7_7_7.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
			migrateFileSystemObject7_7_7.setName(ServiceFatoryConstant.INFRA_SG_SERVICE_CALLER_PREACTION_XSLT);
			migrateFileSystemObject7_7_7.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
			
			MigrateFileSystemObject migrateFileSystemObject7_7_8 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject7_7_8.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
			migrateFileSystemObject7_7_8.setName(ServiceFatoryConstant.INFRA_SG_SET_SG_CONFIG_VAR_XSLT);
			migrateFileSystemObject7_7_8.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
			
			
			MigrateFileSystemObject migrateFileSystemObject7_7_9 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject7_7_9.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_XSLT);
			migrateFileSystemObject7_7_9.setName(ServiceFatoryConstant.INFRA_SG_BATCH_PATH_EVALUATOR_XSLT);
			migrateFileSystemObject7_7_9.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_XSLT);
		}
		
		Set<String> dpaFiles = uploadedFiles.getDpaFiles();
		
		for (String fileName : dpaFiles) {
			MigrateFileSystemObject migrateFileSystemObject8 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject8.setSourceFolder(SourceFolderType.SRC_DPA_DIR);
			migrateFileSystemObject8.setName(fileName);
			migrateFileSystemObject8.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_DPA);
		}
		
		
		MigrateFileSystemObject migrateFileSystemObject10 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject10.setSourceFolder(SourceFolderType.SRC_DIR_APPLIANCE_CERT);
		migrateFileSystemObject10.setName(ServiceFatoryConstant.CERT_IBIT_SF_PRIVATE_KEY);
		migrateFileSystemObject10.setTargetFolder(TargetFolderType.CERT);
		
		MigrateFileSystemObject migrateFileSystemObject11 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject11.setSourceFolder(SourceFolderType.SRC_DIR_APPLIANCE_CERT);
		migrateFileSystemObject11.setName(ServiceFatoryConstant.CERT_IBIT_SF_SSCERT);
		migrateFileSystemObject11.setTargetFolder(TargetFolderType.CERT);
		
		MigrateFileSystemObject migrateFileSystemObject12 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject12.setSourceFolder(SourceFolderType.SRC_DIR_APPLIANCE_CERT);
		migrateFileSystemObject12.setName(ServiceFatoryConstant.CERT_SECURE_SERVICE_FACTORY_CERT);
		migrateFileSystemObject12.setTargetFolder(TargetFolderType.CERT);
		
		List<String> wsdlFiles = serviceDetail.getWsdlFiles();
		for ( String wsdlFile : wsdlFiles) {
			MigrateFileSystemObject migrateFileSystemObject13 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject13.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_WSDL);
			migrateFileSystemObject13.setName(wsdlFile);
			migrateFileSystemObject13.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_WSDL);
		}
		
		List<String> xsdFiles = serviceDetail.getXsdFiles();
		for ( String xsdFile : xsdFiles) {
			MigrateFileSystemObject migrateFileSystemObject14 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject14.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_XSD);
			migrateFileSystemObject14.setName(xsdFile);
			migrateFileSystemObject14.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_XSD);
		}
		
		MigrateFileSystemObject migrateFileSystemObject15 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject15.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_XML);
		migrateFileSystemObject15.setName(ServiceFatoryConstant.ServiceFactory_ServiceConfig_File);
		migrateFileSystemObject15.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_XML);
		
		MigrateFileSystemObject migrateFileSystemObject28 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject28.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_XML);
		migrateFileSystemObject28.setName(ServiceFatoryConstant.ServiceFactory_AAA_CONFIG_FILE);
		migrateFileSystemObject28.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_XML);
		
		MigrateFileSystemObject migrateFileSystemObject2_9 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject2_9.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_XML);
		migrateFileSystemObject2_9.setName(ServiceFatoryConstant.ServiceFactory_DATA_VALIDATION_CONFIG_FILE);
		migrateFileSystemObject2_9.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_XML);
		
		MigrateFileSystemObject migrateFileSystemObject2_11 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject2_11.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_XML);
		migrateFileSystemObject2_11.setName(ServiceFatoryConstant.ServiceFactory_LOG_CONFIG_FILE);
		migrateFileSystemObject2_11.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_XML);
		
		
		Set<String> testResFiles = uploadedFiles.getTestResFiles();
		
		for (String fileName : testResFiles) {
			MigrateFileSystemObject migrateFileSystemObject16 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject16.setSourceFolder(SourceFolderType.SRC_TEST_DIR);
			migrateFileSystemObject16.setName(fileName);
			migrateFileSystemObject16.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_TEST);
		}
		
		
		MigrateFileSystemObject migrateFileSystemObject17 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject17.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_ODBC_XSLT);
		migrateFileSystemObject17.setName(ServiceFatoryConstant.INFRA_SET_ODBC_VAR_XSLT);
		migrateFileSystemObject17.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_ODBC_XSLT);
		
		MigrateFileSystemObject migrateFileSystemObject18 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject18.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_ODBC_XSLT);
		migrateFileSystemObject18.setName(ServiceFatoryConstant.INFRA_QUERY_RESPONDER_XSLT);
		migrateFileSystemObject18.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_ODBC_XSLT);
		
		Set<String> xsltFiles = uploadedFiles.getXsltFiles();
		for (String fileName : xsltFiles) {
			MigrateFileSystemObject migrateFileSystemObject19 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
			migrateFileSystemObject19.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_XSLT);
			migrateFileSystemObject19.setName(fileName);
			migrateFileSystemObject19.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_XSLT);
		}
				
		
		
		MigrateFileSystemObject migrateFileSystemObject21 = migrateFileSystemObjects.addNewMigrateFileSystemObject();			
		migrateFileSystemObject21.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_RESPONDER);
		migrateFileSystemObject21.setName(ServiceFatoryConstant.INFRA_SET_RESPONSE_PLAYLOAD_XML_XSLT);
		migrateFileSystemObject21.setTargetFolder(TargetFolderType.REPOSITORY_SERVICE_NAME_INFRA_ODBC_XSLT);
		
		 System.out.println("out===" + manifestDoc.toString());		 
		 ServiceFactoryFolderStructure serviceFolder = serviceDetail.getServiceFolder();
		
		 File outputFile = new File(serviceFolder.getManitestDir() + "/" +  ServiceFatoryConstant.SF_DEPLOYMENT_MANIFEST_File );
		 XmlOptions xmlOptions = new XmlOptions();
	     xmlOptions.setSavePrettyPrint();
	 
	     WSDLOperationsUtil.removeNamespaces(manifestDoc);
		 manifestDoc.save(outputFile, xmlOptions);
		
	}
	
	public void createDeploymentManifestUT(ServiceDetail serviceDetail) throws IOException {
		ManifestDocument manifestDoc = null;
		manifestDoc = ManifestDocument.Factory.newInstance();
		Manifest manifest = manifestDoc.addNewManifest();
		manifest.setDomain(serviceDetail.getEnvironment().getDomainName());
		manifest.setEnv(serviceDetail.getEnvironment().getEnvironmentName());
		
		MigrateFileSystemObjects migrateFileSystemObjects = manifest.addNewMigrateFileSystemObjects();
		MigrateFileSystemObject migrateFileSystemObject1 = migrateFileSystemObjects.addNewMigrateFileSystemObject();
				
		migrateFileSystemObject1.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_RESPONDER);
		migrateFileSystemObject1.setName("responder-convert-output.ffd");
		migrateFileSystemObject1.setTargetFolder(TargetFolderType.REPOSITORY_INFRASTRUCTURE_RESPONDER);
		
		MigrateFileSystemObject migrateFileSystemObject2 = migrateFileSystemObjects.addNewMigrateFileSystemObject();
		
		migrateFileSystemObject2.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_RESPONDER);
		migrateFileSystemObject2.setName("set-response-payload.xslt");
		migrateFileSystemObject2.setTargetFolder(TargetFolderType.REPOSITORY_INFRASTRUCTURE_RESPONDER);
		
		MigrateFileSystemObject migrateFileSystemObject3 = migrateFileSystemObjects.addNewMigrateFileSystemObject();
		migrateFileSystemObject3.setSourceFolder(SourceFolderType.BUILD_DIR_REPOSITORY_INFRA_RESPONDER);
		migrateFileSystemObject3.setName("set-response-payload-xml.xslt");
		migrateFileSystemObject3.setTargetFolder(TargetFolderType.REPOSITORY_INFRASTRUCTURE_RESPONDER);
		
		
		 ServiceFactoryFolderStructure serviceFolder = serviceDetail.getServiceFolder();
		 File outputFile = new File(serviceFolder.getManitestDir() + "/" + ServiceFatoryConstant.SF_DEPLOYMENT_MANIFEST_UT_File );
		 XmlOptions xmlOptions = new XmlOptions();
	     xmlOptions.setSavePrettyPrint();
	    
	     WSDLOperationsUtil.removeNamespaces(manifestDoc);
		 manifestDoc.save(outputFile, xmlOptions);
		 System.out.println("out===" + manifestDoc.toString());
	}
	
}
