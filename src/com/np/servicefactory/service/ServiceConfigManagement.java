/*
 *   Create Service Config File  
 */
package com.np.servicefactory.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.bns.ibit.servicefactory.template.serviceconfig.v1.ProviderDocument.Provider;
import com.bns.ibit.servicefactory.template.serviceconfig.v1.ProvidersDocument.Providers;
import com.bns.ibit.servicefactory.template.serviceconfig.v1.ServiceConfigDocument;
import com.bns.ibit.servicefactory.template.serviceconfig.v1.ServiceConfigDocument.ServiceConfig;
import com.np.servicefactory.model.EnvironmentSpecificProperties;
import com.np.servicefactory.model.OperationDetail;
import com.np.servicefactory.model.ServiceDetail;
import com.np.servicefactory.model.ServiceEnvironment;
import com.np.servicefactory.model.ServiceFactoryFolderStructure;
import com.np.servicefactory.model.ServiceProvider;
import com.np.servicefactory.util.PropertiesConfigUtil;
import com.np.servicefactory.util.ServiceFatoryConstant;
import com.np.servicefactory.util.WSDLOperationsUtil;


/**
 * @author Wang
 *
 */

@Service("serviceConfigManagement")

public class ServiceConfigManagement {

	private static final Logger logger = Logger.getLogger(ServiceConfigManagement.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ServiceConfigManagement s = new ServiceConfigManagement();
		
		ServiceDetail serviceFactoryForm = new ServiceDetail();
		
		OperationDetail sb= new OperationDetail();
		
		ArrayList<ServiceProvider> serviceProviders = new ArrayList();
		
		
		
		ServiceProvider serviceProvider1 = new ServiceProvider ();
		
	//	serviceProvider1.setBackendUrl("12131.234324");
		serviceProvider1.setInputTransformation("/in/afaf/afda.dpa");
		serviceProvider1.setOutputTransformation("/out/afaf/afda.dpa");
		serviceProvider1.setRoutingRule("afasfasafa");
		
		
		
		serviceProviders.add(serviceProvider1);
		
		
		ServiceProvider serviceProvider2 = new ServiceProvider ();
		
	//	serviceProvider2.setBackendUrl("12131.2343243");
		//serviceProvider2.setSqlDatasourceName("afafdaf");
		//serviceProvider2.setSqlInputArguments("afafhaf");;
		//serviceProvider2.setSqlResultsetTransform("afafsa");
		
		//serviceProvider2.setSqlStatement("select afajdf");
	
		
		serviceProviders.add(serviceProvider2);
		
		
		sb.setProviders(serviceProviders);
		
		serviceFactoryForm.setServiceName("searchCustomer");
		//serviceFactoryForm.setServiceStatus("enbled");
		//serviceFactoryForm.setServiceTargetNameSpace("afjalkfja");
		

		
		//s.createServiceConfigFile(serviceFactoryForm);
	}

	/*
	 *   Create Service Config File for DP 
	 */
	
	public void createServiceConfigFile(ServiceDetail serviceDetail) {
		ServiceConfigDocument serviceConfigDoc = null;

		try {

			serviceConfigDoc = ServiceConfigDocument.Factory.newInstance();
			ServiceConfig newServiceConfig = serviceConfigDoc.addNewServiceConfig();
			com.bns.ibit.servicefactory.template.serviceconfig.v1.ServiceDocument.Service service = newServiceConfig.addNewService();

			
			if (serviceDetail.getServiceStatus()==null){
				service.setStatus(ServiceFatoryConstant.ServiceFactory_Config_File_Status);
			}else {
				service.setStatus(serviceDetail.getServiceStatus());
			}
				
			
			service.setName(serviceDetail.getServiceName());
			
			service.setTargetnamespace(serviceDetail.getServiceTargetNameSpace());
			
			
			Providers providers = service.addNewProviders();

			List<OperationDetail> operationDetails = serviceDetail.getOperationDetails();
			ServiceEnvironment environment = serviceDetail.getEnvironment();
			String dpaFileFolder = ServiceFatoryConstant.REPOSITORY_TOKEN + ServiceFatoryConstant.SERVICE_NAME_TOKEN  +"/dpa/";
			String xsltFileFolder = ServiceFatoryConstant.REPOSITORY_TOKEN +  ServiceFatoryConstant.SERVICE_NAME_TOKEN +"/xslt/";
			String defaultVersionGatewayTransformFile = ServiceFatoryConstant.REPOSITORY_TOKEN + "infrastructure"+"/xslt/" + ServiceFatoryConstant.INFRA_IDENTITY_XSLT;
			for(OperationDetail operationDetail : operationDetails) {
				String pattern = operationDetail.getPattern();
				String operationName = operationDetail.getOperationName();
				List<ServiceProvider> serviceProviders = operationDetail.getProviders();
				for(ServiceProvider serviceProvider : serviceProviders) {
					Provider p =providers.addNewProvider();
					p.setOperation(operationName);
					if(ServiceFatoryConstant.OPERATION_PATTERN_VERSION_GATEWAY.equalsIgnoreCase(pattern)) {
						if(!StringUtils.isBlank(serviceProvider.getInputTransformation())) {
							p.setInputTransformation(xsltFileFolder + serviceProvider.getInputTransformation());	
						} else {
							p.setInputTransformation(defaultVersionGatewayTransformFile);
						}
						
						if(!StringUtils.isBlank(serviceProvider.getOutputTransformation())) {
							p.setOutputTransformation(xsltFileFolder + serviceProvider.getOutputTransformation());	
						} else {
							p.setOutputTransformation(defaultVersionGatewayTransformFile);
						}
					} else {
						if (!StringUtils.isBlank(serviceProvider.getInputTransformation())) {
							p.setInputTransformation(dpaFileFolder + serviceProvider.getInputTransformation());
						}

						if (!StringUtils.isBlank(serviceProvider.getOutputTransformation())) {
							p.setOutputTransformation(dpaFileFolder
									+ serviceProvider.getOutputTransformation());
						}
					}
					if (serviceProvider.getBackEndUrlKey() != null
							&& !serviceProvider.getBackEndUrlKey().equals("")) {
						p.setBackendUrl("@"
								+ serviceProvider.getBackEndUrlKey() + "@");
					}
					
					p.setPattern(pattern);
					
					if (serviceProvider.getRoutingRule()!=null && !serviceProvider.getRoutingRule().equals("")){
						p.setRoutingRule(serviceProvider.getRoutingRule());
					}	else {
						p.setRoutingRule(ServiceFatoryConstant.DEFAULT_ROUTING_RULE);
					}
					
					
					if (!StringUtils.isBlank(serviceProvider.getSqlInputArgumentsTransFileName())){
						p.setSqlInputArguments(xsltFileFolder + serviceProvider.getSqlInputArgumentsTransFileName());
					}
					
					if (!StringUtils.isBlank(serviceProvider.getSqlResultsetTransformFileName())){
						p.setSqlResultsetTransform(xsltFileFolder + serviceProvider.getSqlResultsetTransformFileName());
					}
					
					if (!StringUtils.isBlank(serviceProvider.getSqlDatasourceNameKey())){
						p.setSqlDatasourceName("@"+serviceProvider.getSqlDatasourceNameKey() + "@");
					}
					
					if (!StringUtils.isBlank(serviceProvider.getSqlStatementKey())){
						p.setSqlStatement("@"+serviceProvider.getSqlStatementKey() + "@");
					}
				}
			}
			System.out.println("xml string ==" + serviceConfigDoc.toString());
			ServiceFactoryFolderStructure serviceFolder = serviceDetail.getServiceFolder();
			File outputFile = new File(serviceFolder.getXmlDir() + "/" + ServiceFatoryConstant.ServiceFactory_ServiceConfig_File );
			XmlOptions xmlOptions = new XmlOptions();
		    xmlOptions.setSavePrettyPrint();
		  
		    WSDLOperationsUtil.removeNamespaces(serviceConfigDoc);
		    
		
		    serviceConfigDoc.save(outputFile, xmlOptions);

		} catch (Exception ex) {
			ex.getMessage();
		}

	}
	
	public void addAdditionalPart(ServiceDetail serviceDetail) {
		System.out.println("Add additonal part in serviceXml");
		
		ServiceFactoryFolderStructure sfStructrue = serviceDetail.getServiceFolder();
		String fileTarget = sfStructrue.getXmlDir() + "/" + ServiceFatoryConstant.ServiceFactory_ServiceConfig_File;
		try {
			String line = null;
			ArrayList<String> lines = new ArrayList<String>();
			File f1 = new File(fileTarget);
			FileReader fr = new FileReader(f1 );
			BufferedReader br = new BufferedReader(fr);
		
            boolean addAdditonalPartFlag = false;
            boolean isAdditionalPartAdded = false;
            
			while ((line = br.readLine()) != null) {
								
				if(addAdditonalPartFlag && !isAdditionalPartAdded) {
					lines.add(ServiceFatoryConstant.aditionalPart_1);
					lines.add(ServiceFatoryConstant.aditionalPart_2);
					lines.add(ServiceFatoryConstant.aditionalPart_3);
					lines.add(ServiceFatoryConstant.aditionalPart_4);
					isAdditionalPartAdded = true;
				}
				
				String lowercaseLine = line.toLowerCase();
				
				if (lowercaseLine.indexOf("service-config") != -1) {
					addAdditonalPartFlag = true;
				}
				
				lines.add(line);
			}
			
			
			
			FileWriter fw = new FileWriter(f1, false);
	        BufferedWriter out = new BufferedWriter(fw);
	        PrintWriter pr = new PrintWriter(out);
	        for(String eachLine : lines) {
	        	pr.println(eachLine);	        
	        	
	        }
	       			
	        br.close();
			br = null;
	        pr.close();
	        out = null;

		} catch (java.io.FileNotFoundException ex) {
			logger.error(ex);	
		} catch (java.io.IOException ex) {
			logger.error(ex);	
			ex.printStackTrace();
		}
	}

}
