package com.np.servicefactory.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.np.servicefactory.controller.EnvironmentPropertyController;
import com.np.servicefactory.model.OperationDetail;
import com.np.servicefactory.model.ServiceDetail;
import com.np.servicefactory.model.ServiceEnvironment;
import com.np.servicefactory.model.ServiceFactoryFolderStructure;
import com.np.servicefactory.model.ServiceProvider;
import com.np.servicefactory.model.TestConfiguration;
import com.np.servicefactory.model.UploadedFiles;
import com.np.servicefactory.util.ServiceFatoryConstant;
import com.np.servicefactory.util.WSDLOperationsUtil;

@Service("serviceFacotryHelpService")
@PropertySource("classpath:com/np/servicefactory/config/config.properties")
public class ServiceFacotryHelpService {
	private static int dirNameIdentifier = 0; 
	static final String sfTempDirName="service_factory_temp";
	final String WSDL_SUFFIX = "WSDL";
	final String XSD_SUFFIX = "XSD";
	@Autowired
	Environment env;
	private static final Logger logger = Logger.getLogger(ServiceFacotryHelpService.class);
	

	public ServiceFactoryFolderStructure createDirStructrue() {
		ServiceFactoryFolderStructure folderStructure = new ServiceFactoryFolderStructure();
		String tempDirName =    sfTempDirName + "_" + dirNameIdentifier + "/";
		if(dirNameIdentifier == 10) {
			dirNameIdentifier = 0;
		}
		dirNameIdentifier = dirNameIdentifier + 1;
		String workingDir =  env.getProperty("home.dir") + tempDirName;
		File file = new File(workingDir);
		
		
		if(	file.isDirectory()) {
			deleteDir(file);
		}
		
		//Create working directory
	
		if (file.mkdir()) {
			logger.info("Directory is created!");
		} else {
			logger.info("Failed to create directory!");
		}
	
		
		folderStructure.setWorkingDir(workingDir);
		
		//Create root directory
        String tmpDir = workingDir + env.getProperty("dir.tmp");
		
		File tmpDirFile = new File(tmpDir);
		tmpDirFile.mkdir();
		folderStructure.setTmpDir(tmpDir);
		
		String srcDir = workingDir + env.getProperty("src.dir") + "/";		
		folderStructure.setSrcDir(srcDir);
		
		File srcDirFile = new File(srcDir);		
		srcDirFile.mkdir();
		
        String configDir = srcDir + env.getProperty("sub.dir.config");
        folderStructure.setConfigDir(configDir);
        
        File configDirFile = new File(configDir);
        configDirFile.mkdir();
        
        
        String dpaDir = srcDir + env.getProperty("sub.dir.dpa");	
        folderStructure.setDpaDir(dpaDir);
		File dpaDirFile = new File(dpaDir);
		dpaDirFile.mkdir();
		
		
		String infrastructureDir = srcDir + env.getProperty("sub.dir.infrastructure");
		folderStructure.setInfrastructureDir(infrastructureDir);
		
		String applianceDir= infrastructureDir + "/" +env.getProperty("sub.dir.infrastructure.appliance");
		folderStructure.setApplianceDir(applianceDir);
		
		String certDir = applianceDir + "/" + env.getProperty("sub.dir.infrastructure.appliance.cert");
		folderStructure.setCertDir(certDir);
		
		File certDirFile = new File(certDir);		
		certDirFile.mkdirs();	
		
		String manitestDir = srcDir + env.getProperty("sub.dir.manitest");
		folderStructure.setManitestDir(manitestDir);
		
		File manitestDirFile = new File(manitestDir);
		manitestDirFile.mkdir();
		
		
		String testDir = srcDir + env.getProperty("sub.dir.test");
		folderStructure.setTestDir(testDir);
		
		File testDirFile = new File(testDir);
		testDirFile.mkdir();
		
		String testReqDir = testDir +"/"+ env.getProperty("sub.dir.test.request");
		folderStructure.setTestReqDir(testReqDir);
		
		File testReqDirFile = new File(testReqDir);
		testReqDirFile.mkdir();
		
		String testResDir = testDir +"/"+ env.getProperty("sub.dir.test.response");
		folderStructure.setTestResDir(testResDir);
		
		File testResDirFile = new File(testResDir);
		testResDirFile.mkdir();
				
		
		//Create sub dir		
		String wsdlDir = srcDir + env.getProperty("sub.dir.wsdl");
		folderStructure.setWsdlDir(wsdlDir);
		
		File wsdlDirFile = new File(wsdlDir);
		wsdlDirFile.mkdir();
		
		
		String xmlDir = srcDir + env.getProperty("sub.dir.xml");
		folderStructure.setXmlDir(xmlDir);
		File xmlDirFile = new File(xmlDir);
		xmlDirFile.mkdir();
		
		
		String xsdDir = srcDir + env.getProperty("sub.dir.xsd");
		folderStructure.setXsdDir(xsdDir);
		File xsdDirFile = new File(xsdDir);
		xsdDirFile.mkdir();
		
		String xsltDir = srcDir + env.getProperty("sub.dir.xslt");
		folderStructure.setXsltDir(xsltDir);
		File xsltDirFile = new File(xsltDir);
		
		xsltDirFile.mkdir();
		try {
			copyProtoTypeFilesToSrcFolder(folderStructure);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return folderStructure;
	}	
	
public void copyProtoTypeFilesToSrcFolder(ServiceFactoryFolderStructure folderStructure ) throws IOException {
		 
		
		
		String protoTypeFileDir = env.getProperty("service.prototype.dir");
		
		String privKeyFile = protoTypeFileDir + "/" + ServiceFatoryConstant.CERT_IBIT_SF_PRIVATE_KEY;
		String servicFactorySScert =protoTypeFileDir + "/" + ServiceFatoryConstant.CERT_IBIT_SF_SSCERT;
		String customerSert = protoTypeFileDir + "/" +ServiceFatoryConstant.CERT_SECURE_SERVICE_FACTORY_CERT;
		
		
		File soruceFile = new File(privKeyFile);
		String targetDir = folderStructure.getCertDir();
		
		
		String targetFileName = targetDir + "/" + ServiceFatoryConstant.CERT_IBIT_SF_PRIVATE_KEY;
		
		File targetFile = new File(targetFileName);
		
		FileUtils.copyFile(soruceFile, targetFile);
		
		
		soruceFile = new File(servicFactorySScert);
	
		
		
	    targetFileName = targetDir + "/" + ServiceFatoryConstant.CERT_IBIT_SF_SSCERT;
		
		targetFile = new File(targetFileName);
		
		FileUtils.copyFile(soruceFile, targetFile);
		
		
		soruceFile = new File(customerSert);	
		
	    targetFileName = targetDir + "/" + ServiceFatoryConstant.CERT_SECURE_SERVICE_FACTORY_CERT;
		
		targetFile = new File(targetFileName);
		
		FileUtils.copyFile(soruceFile, targetFile);
		
		targetFileName = targetDir + "/" + ServiceFatoryConstant.CERT_SECURE_SERVICE_FACTORY_PRIVATE_KEY;			
		targetFile = new File(targetFileName);			
		FileUtils.copyFile(soruceFile, targetFile);
		
		
		
		
		String sfAAAConfigFile = protoTypeFileDir + "/" + ServiceFatoryConstant.ServiceFactory_AAA_CONFIG_FILE;
		soruceFile = new File(sfAAAConfigFile);
		
		targetFileName = folderStructure.getXmlDir() + "/" + ServiceFatoryConstant.ServiceFactory_AAA_CONFIG_FILE;
		targetFile = new File(targetFileName);
		
		FileUtils.copyFile(soruceFile, targetFile);
		
		String dataValidationConfigFile = protoTypeFileDir + "/" + ServiceFatoryConstant.ServiceFactory_DATA_VALIDATION_CONFIG_FILE;
		soruceFile = new File(dataValidationConfigFile);
		
		targetFileName = folderStructure.getXmlDir() + "/" + ServiceFatoryConstant.ServiceFactory_DATA_VALIDATION_CONFIG_FILE;
		targetFile = new File(targetFileName);
		
		FileUtils.copyFile(soruceFile, targetFile);
		
		String logConfigFile = protoTypeFileDir + "/" + ServiceFatoryConstant.ServiceFactory_LOG_CONFIG_FILE;
		soruceFile = new File(logConfigFile);
		
		targetFileName = folderStructure.getXmlDir() + "/" + ServiceFatoryConstant.ServiceFactory_LOG_CONFIG_FILE;
		targetFile = new File(targetFileName);
		
		FileUtils.copyFile(soruceFile, targetFile);
	}
	
	
	
	public void processWSDLAndXSDfiles(ServiceFactoryFolderStructure folderStructure, ServiceDetail serviceDetail) {
		List<String> wsdlFiles = new ArrayList<String>();
		List<String> xsdFiles = new ArrayList<String>();
		
		String tmpDir = folderStructure.getTmpDir();
		File path = new File(tmpDir);
		String wsdldir = folderStructure.getWsdlDir();
		String schemaFileDir = folderStructure.getXsdDir();
		
		processFile(path, wsdldir, schemaFileDir, wsdlFiles, xsdFiles  );
		
		serviceDetail.setWsdlFiles(wsdlFiles);
		serviceDetail.setXsdFiles(xsdFiles);

	}
	
	private void processFile(File folder, String wsdldir, String schemaFileDir, List<String> wsdlFiles, List<String> xsdFiles ) {
		File[] files = folder.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile()) { 
				logger.info(files[i]);
				String fileName = files[i].getName();
				if(fileName.length() < 4) {
					continue;
				}
				String wsdlSuffix = fileName.substring(fileName.length() - 4);
				String xsdSuffix = fileName.substring(fileName.length() -3);
				if (WSDL_SUFFIX.equalsIgnoreCase(wsdlSuffix)) {
					WSDLOperationsUtil.replaceSchemaLoc(
							files[i].getAbsolutePath(), wsdldir + "/"
									+ fileName, true);
					wsdlFiles.add(fileName);
				} else if (XSD_SUFFIX.equalsIgnoreCase(xsdSuffix)){
					WSDLOperationsUtil.replaceSchemaLoc(
							files[i].getAbsolutePath(), schemaFileDir + "/"
									+ fileName, false);
					xsdFiles.add(fileName);
				}
			} else {
				processFile(files[i], wsdldir, schemaFileDir, wsdlFiles, xsdFiles  );
			}
		}
	}
	

	public void deleteDir(File file) {
		if(file.isDirectory()){
			 
    		//directory is empty, then delete it
    		if(file.list().length==0){
    			
    		   file.delete();
    		   logger.info("Directory is deleted : " 
                                                 + file.getAbsolutePath());
    			
    		}else{
    			
    		   //list all the directory contents
        	   String files[] = file.list();
     
        	   for (String temp : files) {
        	      //construct the file structure
        	      File fileDelete = new File(file, temp);
        		 
        	      //recursive delete
        	     this.deleteDir(fileDelete);
        	   }
        		
        	   //check the directory again, if empty then delete it
        	   if(file.list().length==0){
           	     file.delete();
        	     logger.info("Directory is deleted : " 
                                                  + file.getAbsolutePath());
        	   }
    		}
    		
    	}else{
    		//if file, then delete it
    		file.delete();
    		logger.info("File is deleted : " + file.getAbsolutePath());
    	}
    
	}
	
	public void cleanupUploadedfiles(ServiceDetail serviceDetail, UploadedFiles uploadedFiles) {
		
		Set<String> latest_dpaFiles = new HashSet<String>();
		Set<String> latest_testReqFiles = new HashSet<String>();
		Set<String> latest_testResFiles = new HashSet<String>();
		Set<String> latest_xsltFiles = new HashSet<String>();
		
		List<OperationDetail> operationDetails = serviceDetail.getOperationDetails();
		
		for (OperationDetail operationDetail : operationDetails) {
			List<ServiceProvider> providers =  operationDetail.getProviders();
			List<TestConfiguration> testConfigs = operationDetail.getTestConfigs();
			for(ServiceProvider provider : providers) {
				String inputTransformation = provider.getInputTransformation();
				String outputTransformation = provider.getOutputTransformation();
				String sqlInputArgument = provider.getSqlInputArgumentsTransFileName();
				String sqlResultSetTransForm = provider.getSqlResultsetTransformFileName();				
				
				if(!StringUtils.isBlank(inputTransformation)) {
					latest_dpaFiles.add(inputTransformation);
					uploadedFiles.removeFileToDPASet(inputTransformation);
				}
				
				if(!StringUtils.isBlank(outputTransformation)) {
					latest_dpaFiles.add(outputTransformation);
					uploadedFiles.removeFileToDPASet(outputTransformation);
				}
				
				if(!StringUtils.isBlank(sqlInputArgument)) {
					latest_xsltFiles.add(sqlInputArgument);
					uploadedFiles.removeFileToXSLTSet(sqlInputArgument);
				}
				
				if(!StringUtils.isBlank(sqlResultSetTransForm)) {
					latest_xsltFiles.add(sqlResultSetTransForm);
					uploadedFiles.removeFileToXSLTSet(sqlResultSetTransForm);				
				}
			}
			
			for (TestConfiguration testConfig : testConfigs) {
				String resFileName = testConfig.getResponseFileName();
				String reqFileName = testConfig.getSampleSoapRequestFileName();
				
				if(!StringUtils.isBlank(reqFileName)) {
					latest_testReqFiles.add(reqFileName);
					uploadedFiles.removeFileToTestReqSet(reqFileName);
				}
				
				if(!StringUtils.isBlank(resFileName)) {
					latest_testResFiles.add(resFileName);
					uploadedFiles.removeFileToTestResSet(resFileName);
				}
			}
		}
		
		ServiceFactoryFolderStructure folderStructure = serviceDetail.getServiceFolder();
		String dpaDir = folderStructure.getDpaDir();
		String xsltDir = folderStructure.getXsltDir();
		String reqDir = folderStructure.getTestReqDir();
		String resDir = folderStructure.getTestResDir();
		
		cleanUpFolder(dpaDir, uploadedFiles.getDpaFiles());
		cleanUpFolder(xsltDir, uploadedFiles.getXsltFiles());
		cleanUpFolder(reqDir, uploadedFiles.getTestReqFiles());
		cleanUpFolder(resDir, uploadedFiles.getTestResFiles());
		
		uploadedFiles.setDpaFiles(latest_dpaFiles);
		uploadedFiles.setXsltFiles(latest_xsltFiles);
		uploadedFiles.setTestReqFiles(latest_testReqFiles);
		uploadedFiles.setTestResFiles(latest_testResFiles);
		cleanUpWSDLFile(serviceDetail);
	}
	
	private void cleanUpWSDLFile(ServiceDetail serviceDetail) {
		Set<String> depentXSDFiles = serviceDetail.getServiceDependentXSDFiles();
		List<String> xsdFiles = serviceDetail.getXsdFiles();
		List<String> wsdlFiles = serviceDetail.getWsdlFiles();
		
		ServiceFactoryFolderStructure folderStructure = serviceDetail.getServiceFolder();
		String xsdDir = folderStructure.getXsdDir();
		String wsdlDir = folderStructure.getWsdlDir();
		
		for(String fileName : xsdFiles) {
			if(!depentXSDFiles.contains(fileName)) {
				File file = new File(xsdDir + "/" + fileName);
				file.delete();
			}
		}
		String rootWsdlName = serviceDetail.getRootWsdlName();
		for(String fileName : wsdlFiles) {
			if(!rootWsdlName.equalsIgnoreCase(fileName)) {
				File file = new File(wsdlDir + "/" + fileName);
				file.delete();
			}
		}
	}

	public void resetWSDAndXSDFileList(ServiceDetail serviceDetail) {
		List<String> wsdlFiles = serviceDetail.getWsdlFiles();
		List<String> xsdFiles = serviceDetail.getXsdFiles();
		wsdlFiles.clear();
		wsdlFiles.add(serviceDetail.getRootWsdlName());
		
		xsdFiles.clear();
		xsdFiles.addAll(serviceDetail.getServiceDependentXSDFiles());
		
	}
	
	private void cleanUpFolder(String dir, Set<String> removedFiles) {
		for (String fileName : removedFiles) {
		    File file = new File(dir + "/" + fileName);
		    file.delete();
		}
	}

	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}
	
	public static void processLogFileAndDataValidationFile(String sourceFilePath, ServiceDetail serviceDetail) {

		System.out.println("sourceFilePath: " +  sourceFilePath);
		try {
			
			BufferedReader br = new BufferedReader(new FileReader(sourceFilePath));
			            
			String line ="", oldText="";
			while ((line = br.readLine()) != null) {
				
			      oldText +=line +"\r\n";	
			
			}			
			br.close();
			String newText = oldText.replaceAll( "serviceName", serviceDetail.getServiceName());
			newText = newText.replaceAll("nameSpace", serviceDetail.getServiceTargetNameSpace());
			
			FileWriter writer = new FileWriter(sourceFilePath);
			writer.write(newText); writer.close();

			
		} catch (java.io.FileNotFoundException ex) {
			logger.error(ex);	
		} catch (java.io.IOException ex) {
			logger.error(ex);	
			ex.printStackTrace();
		}

	}
	
	public void copySGConfigFileToXmlFolder(ServiceDetail serviceDetail) {
		List<OperationDetail> operationDetails =serviceDetail.getOperationDetails();
		boolean isCompositeService = false;
		
		for(OperationDetail operationDetail : operationDetails) {
			if("true".equals(operationDetail.getCompositeServiceFlag())) {
				isCompositeService = true;
				break;
			}
		}
		
		if(!isCompositeService)  return;
		
		ServiceFactoryFolderStructure folderStructure = serviceDetail.getServiceFolder();
		
		String protoTypeFileDir = env.getProperty("service.prototype.dir");
		
		String sgConfigFile = protoTypeFileDir + "/" + ServiceFatoryConstant.ServiceFactory_SG_CONFIG_XML;
	
		
		
		File soruceFile = new File(sgConfigFile);
		String targetDir = folderStructure.getXmlDir();
		
		
		String targetFileName = targetDir + "/" + ServiceFatoryConstant.ServiceFactory_SG_CONFIG_XML;
		
		File targetFile = new File(targetFileName);
		
		try {
			FileUtils.copyFile(soruceFile, targetFile);
		} catch (IOException e) {			
			e.printStackTrace();
		}
		
		
	
	}
}
