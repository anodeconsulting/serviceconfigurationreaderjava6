/**
 * 
 */
package com.np.servicefactory.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.xmlbeans.XmlOptions;
import org.springframework.stereotype.Service;

import com.bns.ibit.servicefactory.template.responderconfig.v1.OperationDocument.Operation;
import com.bns.ibit.servicefactory.template.responderconfig.v1.ServicesDocument;
import com.bns.ibit.servicefactory.template.responderconfig.v1.ServicesDocument.Services;
import com.np.servicefactory.model.OperationDetail;
import com.np.servicefactory.model.OperationDoc;
import com.np.servicefactory.model.ResponseConfigService;
import com.np.servicefactory.model.ServiceDetail;
import com.np.servicefactory.model.ServiceFactoryFolderStructure;
import com.np.servicefactory.model.TestConfiguration;
import com.np.servicefactory.util.ServiceFatoryConstant;
import com.np.servicefactory.util.WSDLOperationsUtil;




/**
 * @author Wang
 *
 */

@Service("responderConfigManagement")
public class ResponderConfigManagement {

	public static final String SAMPLE_SOAP_REQUEST_FILE_DEFAULT_VALUE="anyURI";
	public static final String SAMPLE_SOAP_DUMMY_REQUEST_FILE="dummyReq.xml";

			
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ResponderConfigManagement r= new ResponderConfigManagement();
		ArrayList<ResponseConfigService> responseConfigServices = new ArrayList<ResponseConfigService>();
		
		r.removeWhiteSpace();
		
		
		OperationDoc operationDoc1 = new OperationDoc();
		
		operationDoc1.setFilename("afa");
		operationDoc1.setLatency(12);
		operationDoc1.setName("afaf");;
		operationDoc1.setPattern("binary");
		operationDoc1.setResponseFilename("afa");
		operationDoc1.setRoutingRule("daf");
		operationDoc1.setSampleSOAPRequestFile("af.dafa");
		
		OperationDoc operationDoc2 = new OperationDoc();
		
		operationDoc2.setFilename("afa1");
		operationDoc2.setLatency(12);
		operationDoc2.setName("afaf");;
		operationDoc2.setPattern("binary");
		operationDoc2.setResponseFilename("afa");
		operationDoc2.setRoutingRule("daf");
		operationDoc2.setSampleSOAPRequestFile("af.dafa");
		
		ArrayList l= new ArrayList();
		
		l.add(operationDoc1);
		l.add(operationDoc2);
		
		ResponseConfigService responseConfigService = new ResponseConfigService();
		responseConfigService.setServiceName("abcd");
		responseConfigService.setOperationDoc(l);
		
		responseConfigServices.add(responseConfigService);
		
		r.createResponseConfigFile(null);
	}
	
	

	public void createResponseConfigFile(ServiceDetail serviceDetail) {
		ServicesDocument servicesDoc = null;
		

		try {
			servicesDoc = ServicesDocument.Factory.newInstance();			
			Services services =servicesDoc.addNewServices();
			
			com.bns.ibit.servicefactory.template.responderconfig.v1.ServiceDocument.Service  service= services.addNewService();
			
			
			List<OperationDetail> operationDetails = serviceDetail.getOperationDetails();
			
			String testReqDir = serviceDetail.getEnvironment().getRepositoryUrl() + serviceDetail.getServiceName() + "/" + "test/";		
			String testResDir = serviceDetail.getEnvironment().getRepositoryUrl() + serviceDetail.getServiceName() + "/" + "test/";	
			for (OperationDetail operationDetail : operationDetails){
				
				service.setName(serviceDetail.getServiceName());				
				
				service.setTargetNamespace(serviceDetail.getServiceTargetNameSpace());
	
				
				List<TestConfiguration> testConfigs = operationDetail.getTestConfigs();
				for(TestConfiguration testConfig : testConfigs) {
					       Operation operation = service.addNewOperation();
					        String latency = testConfig.getLatency();
					        if(!StringUtils.isBlank(latency) && StringUtils.isNumeric(latency)) {
					        	operation.setLatency(Integer.parseInt(latency));
					        } else {
					        	operation.setLatency(0); //set default latency value;
					        }
				            
							operation.setName(operationDetail.getOperationName());
							operation.setPattern(operationDetail.getPattern());
							if(StringUtils.isBlank(testConfig.getResponseFileName())) {
								operation.setResponseFileName(SAMPLE_SOAP_DUMMY_REQUEST_FILE);
							} else {
								operation.setResponseFileName(testResDir + testConfig.getResponseFileName());
							}
							if(StringUtils.isBlank(testConfig.getRoutingRule())) {
								operation.setRoutingRule(ServiceFatoryConstant.DEFAULT_ROUTING_RULE);
							} else {
								operation.setRoutingRule(testConfig.getRoutingRule());
							}							
							
							if(StringUtils.isBlank(testConfig.getSampleSoapRequestFileName())) {
								String  sampleSOAPRequestFile = SAMPLE_SOAP_DUMMY_REQUEST_FILE;
								operation.setSampleSOAPRequestFile(sampleSOAPRequestFile);
							} else {								
								operation.setSampleSOAPRequestFile(testReqDir + testConfig.getSampleSoapRequestFileName()); 
							}
							
						}
			}
			
			 ServiceFactoryFolderStructure serviceFolder = serviceDetail.getServiceFolder();
			 File outputFile = new File(serviceFolder.getXmlDir() + "/" + ServiceFatoryConstant.ServiceFactory_ResponderConfig_File );
			 XmlOptions xmlOptions = new XmlOptions();
		     xmlOptions.setSavePrettyPrint();
		    
			
		     WSDLOperationsUtil.removeNamespaces(servicesDoc);
		     servicesDoc.save(outputFile, xmlOptions);
		   
			System.out.println("out===" + servicesDoc.toString());
			
		} catch(Exception ex){
			ex.getMessage();		
		}
	}
	
	
	
	
	public void removeWhiteSpace(){
		String a="string with                multi spaces ";
		//or this 
		String b= a.replace(" ","");
		
		System.out.println(b);
	}
}
