package com.np.servicefactory.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.np.servicefactory.model.KeyValuePair;
import com.np.servicefactory.model.EnvironmentSpecificProperties;
import com.np.servicefactory.model.FrontSideHandler;
import com.np.servicefactory.model.OperationDetail;
import com.np.servicefactory.model.ServiceDetail;
import com.np.servicefactory.model.ServiceEnvironment;
import com.np.servicefactory.util.PropertiesConfigUtil;

@Service("serviceFactoryPropertyService")
public class ServiceFactoryPropertyService {
	@Autowired
	Environment env;
	static int PORT_NUMBER_BASE = 12525;
	
	public void generatePropertyFile (ServiceDetail serviceDetail ) throws IOException {
		Map<String, String> properties = new HashMap<String, String>();
		
		ServiceEnvironment serviceEnvironment = serviceDetail.getEnvironment();
		List<OperationDetail> operationDetails =serviceDetail.getOperationDetails();
		boolean isCompositeService = false;
		
		for(OperationDetail operationDetail : operationDetails) {
			if("true".equals(operationDetail.getCompositeServiceFlag())) {
				isCompositeService = true;
				break;
			}
		}
		
		/*List<OperationDetail> operationDetails = serviceDetail.getOperationDetails();
		String pattern = null;
	    for(OperationDetail operationDetail : operationDetails) {
			if(pattern == null) {
				pattern = operationDetail.getPattern();
				continue;
			}
			
			if(!pattern.equalsIgnoreCase(operationDetail.getPattern())) {
				pattern = "hybrid";
				break;
			}
		}		*/
		
		//properties.put("entry.deploy-pattern", pattern);
		properties.put("domain", serviceEnvironment.getDomainName());
		properties.put("projectName", serviceEnvironment.getProjectName());
		properties.put("wsdl-name", serviceDetail.getRootWsdlName());
		properties.put("tns-uri", serviceDetail.getServiceTargetNameSpace());
		properties.put("service-name", serviceDetail.getServiceName());
		properties.put("service-port-name", serviceDetail.getPortName());
		properties.put("business-domain", serviceDetail.getBusinessDomain());

		
		
		List<FrontSideHandler> frontSideHandlers = serviceDetail.getFrontSideHandlers();
		Map<String, String> sharedProperties = new HashMap<String, String>();	
		
		int http_port_number = PORT_NUMBER_BASE;
		int https_port_number = PORT_NUMBER_BASE + 1;
		int https_port_oneway_number = PORT_NUMBER_BASE + 2;
		int mpg_port_number =  PORT_NUMBER_BASE + 3;
		
		sharedProperties.put("wsp-http-port",  String.valueOf(http_port_number)  );
		sharedProperties.put("wsp-https-port",  String.valueOf(https_port_number)  );
		sharedProperties.put("wsp-https-port-oneway",  String.valueOf(https_port_oneway_number)  );
		sharedProperties.put("mpgw-port", String.valueOf(mpg_port_number));
		if (isCompositeService) {
			sharedProperties.put("is-composite-service", "y");	
			sharedProperties.put("sg-mpgw-port", String.valueOf(PORT_NUMBER_BASE + 4));
			PORT_NUMBER_BASE = PORT_NUMBER_BASE  + 5;
		} else {
			PORT_NUMBER_BASE = PORT_NUMBER_BASE  + 4;
		}
		
		for(FrontSideHandler frontSideHandler: frontSideHandlers) {
			if(StringUtils.isBlank(frontSideHandler.getName()))  continue;
			if("https".equalsIgnoreCase(frontSideHandler.getType())) {
					sharedProperties.put("https-fsh", frontSideHandler.getName());
				
			}
			
			if("http".equalsIgnoreCase(frontSideHandler.getType())) {
				sharedProperties.put("http-fsh", frontSideHandler.getName());
			}
		}		
		
		String shouldflushCache = "true".equalsIgnoreCase(serviceEnvironment.getShouldFlushCache())? "y" :"n"; 
		sharedProperties.put("entry.flush-cache", shouldflushCache);
		
		List<EnvironmentSpecificProperties> environmentList = serviceEnvironment.getEnvironments();
		
		String dir = serviceDetail.getServiceFolder().getConfigDir();
		
		for(EnvironmentSpecificProperties environmentSpecificProperties : environmentList) {
			
			String environmentName = StringUtils.trim(environmentSpecificProperties.getEnvironmentName());
			String propertyFileName =  PropertiesConfigUtil.getPropertyFileName(environmentName, false);
			
			String propertyPathFileName = dir + "/" + propertyFileName;
			this.addBackendUrlkKeysIntoProperties(environmentSpecificProperties, propertyPathFileName);
			this.addODBCInfoIntoProperties(environmentSpecificProperties, propertyPathFileName);
			
		
			properties.put("env", StringUtils.lowerCase(environmentName));
			PropertiesConfigUtil.updatePropertiesConfig(properties, propertyPathFileName);		
			
			String sharedPropertyFileName = PropertiesConfigUtil.getPropertyFileName(environmentName, true);
			String sharedPropertyPathFileName = dir + "/" + sharedPropertyFileName;
			
			PropertiesConfigUtil.updatePropertiesConfig(sharedProperties, sharedPropertyPathFileName);
		}
		
	}
	
	private void addBackendUrlkKeysIntoProperties(EnvironmentSpecificProperties envProperties, String propertyFileName) throws IOException {
		Map<String, String> newProperties = new HashMap<String, String>();
		
		List<KeyValuePair>  backEndUrls = envProperties.getBackEndUrls();
		
		for(KeyValuePair backendUrl : backEndUrls ) {			
			newProperties.put(backendUrl.getKey(), backendUrl.getValue());			
		}
		PropertiesConfigUtil.addProperties(newProperties, propertyFileName);
	}
	
	private void addODBCInfoIntoProperties(EnvironmentSpecificProperties envProperties, String propertyFileName) throws IOException {
		Map<String, String> newProperties = new HashMap<String, String>();
		
		List<KeyValuePair>  datasourceNames = envProperties.getSqlDataSourceNames();
		
		for(KeyValuePair datasourceName : datasourceNames ) {			
			newProperties.put(datasourceName.getKey(), datasourceName.getValue());			
		}
		List<KeyValuePair>  sqlStatements = envProperties.getSqlStatements();
		for(KeyValuePair sqlStatement : sqlStatements ) {			
			newProperties.put(sqlStatement.getKey(), sqlStatement.getValue());			
		}
		
		PropertiesConfigUtil.addProperties(newProperties, propertyFileName);
	}
	
	public  void createPropertyFileFromProtoType(ServiceDetail serviceDetail) throws IOException {
		String protoTypePropFileName = env.getProperty("service.build.property.prototype.path");
		String protoTypeSharedPropFileName = env.getProperty("service.build.shared.property.prototype.path");
		PropertiesConfigUtil.createPropertyFileFromProtoType(serviceDetail, protoTypePropFileName, protoTypeSharedPropFileName);
		
		  
	}
}
