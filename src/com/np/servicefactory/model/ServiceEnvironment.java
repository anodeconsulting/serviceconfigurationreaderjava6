package com.np.servicefactory.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true) 
public class ServiceEnvironment implements Serializable  {
	private static final long serialVersionUID = 8461753799451747571L;
	private String projectName;
	private String environmentType;
	private String environmentName;
	private String repositoryUrl;
	private String domainName;
	private String shouldFlushCache;
	
	private List<EnvironmentSpecificProperties>  environments = new ArrayList<EnvironmentSpecificProperties>();

	
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getEnvironmentType() {
		return environmentType;
	}
	public void setEnvironmentType(String environmentType) {
		this.environmentType = environmentType;
	}
	public String getEnvironmentName() {
		return environmentName;
	}
	public void setEnvironmentName(String environmentName) {
		this.environmentName = environmentName;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	
	public List<EnvironmentSpecificProperties> getEnvironments() {
		return environments;
	}
	public void setEnvironments(List<EnvironmentSpecificProperties> environments) {
		this.environments = environments;
	}
	public String getShouldFlushCache() {
		return shouldFlushCache;
	}
	public void setShouldFlushCache(String shouldFlushCache) {
		this.shouldFlushCache = shouldFlushCache;
	}
	public String getRepositoryUrl() {
		return repositoryUrl;
	}
	public void setRepositoryUrl(String repositoryUrl) {
		this.repositoryUrl = repositoryUrl;
	}
	
}
