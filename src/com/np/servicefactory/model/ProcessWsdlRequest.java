package com.np.servicefactory.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true) 
public class ProcessWsdlRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	String rootWsdlName;

	public String getRootWsdlName() {
		return rootWsdlName;
	}

	public void setRootWsdlName(String rootWsdlName) {
		this.rootWsdlName = rootWsdlName;
	}
}
