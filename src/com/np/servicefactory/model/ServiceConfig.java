package com.np.servicefactory.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true) 
public class ServiceConfig implements Serializable {
	
	private static final long serialVersionUID = 8945021161735712807L;
	
	public String businessDomain;
	
	public List<OperationDetail> operationDetails;
	public List<FrontSideHandler> frontSideHandlers;
	public List<OperationDetail> getOperationDetails() {
		return operationDetails;
	}
	public void setOperationDetails(List<OperationDetail> operationDetails) {
		this.operationDetails = operationDetails;
	}
	public List<FrontSideHandler> getFrontSideHandlers() {
		return frontSideHandlers;
	}
	public void setFrontSideHandlers(List<FrontSideHandler> frontSideHandlers) {
		this.frontSideHandlers = frontSideHandlers;
	}
	public String getBusinessDomain() {
		return businessDomain;
	}
	public void setBusinessDomain(String businessDomain) {
		this.businessDomain = businessDomain;
	}
}
