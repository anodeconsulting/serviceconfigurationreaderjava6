package com.np.servicefactory.model;

public class OperationDoc {
	public int latency;
	public String filename;
	public String name;
	public String pattern;
	public String responseFilename;
	public String routingRule;
	public String sampleSOAPRequestFile;
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPattern() {
		return pattern;
	}


	public void setPattern(String pattern) {
		this.pattern = pattern;
	}


	public String getResponseFilename() {
		return responseFilename;
	}


	public void setResponseFilename(String responseFilename) {
		this.responseFilename = responseFilename;
	}


	public String getSampleSOAPRequestFile() {
		return sampleSOAPRequestFile;
	}


	public void setSampleSOAPRequestFile(String sampleSOAPRequestFile) {
		this.sampleSOAPRequestFile = sampleSOAPRequestFile;
	}



	public OperationDoc() {
		// TODO Auto-generated constructor stub
	}

	
	public String getRoutingRule() {
		return routingRule;
	}

	public void setRoutingRule(String routingRule) {
		this.routingRule = routingRule;
	}

	public int getLatency() {
		return latency;
	}

	public void setLatency(int latency) {
		this.latency = latency;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}


}
