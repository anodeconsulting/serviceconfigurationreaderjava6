package com.np.servicefactory.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true) 
public class ProcessUploadWSDLRes implements Serializable {
	
	private static final long serialVersionUID = 1L;
	String Status;
	List<String> operationNameList = new ArrayList<String>();
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public List<String> getOperationNameList() {
		return operationNameList;
	}
	public void setOperationNameList(List<String> operationNameList) {
		this.operationNameList = operationNameList;
	}
}
