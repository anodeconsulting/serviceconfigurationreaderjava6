package com.np.servicefactory.model;

public class FileUploadRes {
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
