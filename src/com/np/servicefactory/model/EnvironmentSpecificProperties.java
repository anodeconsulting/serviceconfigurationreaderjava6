package com.np.servicefactory.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true) 
public class EnvironmentSpecificProperties implements Serializable{
	
	private static final long serialVersionUID = 5532725022750629997L;
	
	private String environmentName;
	private String environmentType;
	private List<KeyValuePair> backEndUrls = new ArrayList<KeyValuePair>();
	private List<KeyValuePair> sqlDataSourceNames = new ArrayList<KeyValuePair>();
	private List<KeyValuePair> sqlStatements = new ArrayList<KeyValuePair>();
	
	
	public List<KeyValuePair> getSqlDataSourceNames() {
		return sqlDataSourceNames;
	}
	public void setSqlDataSourceNames(List<KeyValuePair> sqlDataSourceNames) {
		this.sqlDataSourceNames = sqlDataSourceNames;
	}
	public List<KeyValuePair> getSqlStatements() {
		return sqlStatements;
	}
	public void setSqlStatements(List<KeyValuePair> sqlStatements) {
		this.sqlStatements = sqlStatements;
	}
	public String getEnvironmentName() {
		return environmentName;
	}
	public void setEnvironmentName(String environmentName) {
		this.environmentName = environmentName;
	}
	public String getEnvironmentType() {
		return environmentType;
	}
	public void setEnvironmentType(String environmentType) {
		this.environmentType = environmentType;
	}
	public List<KeyValuePair> getBackEndUrls() {
		return backEndUrls;
	}
	public void setBackEndUrls(List<KeyValuePair> backEndUrls) {
		this.backEndUrls = backEndUrls;
	}
}
