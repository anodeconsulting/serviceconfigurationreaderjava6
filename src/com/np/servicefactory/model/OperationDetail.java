/**
 * 
 */
package com.np.servicefactory.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Wang
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true) 
public class OperationDetail implements Serializable {
	
	private static final long serialVersionUID = 7822637464778061190L;

	private String operationName;

	private String pattern;
	
	private String compositeServiceFlag;
	
	private List<ServiceProvider> providers;
	
	private List<TestConfiguration> testConfigs;
		

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}



	public List<ServiceProvider> getProviders() {
		return providers;
	}



	public void setProviders(List<ServiceProvider> providers) {
		this.providers = providers;
	}

	public List<TestConfiguration> getTestConfigs() {
		return testConfigs;
	}
	public void setTestConfigs(List<TestConfiguration> testConfigs) {
		this.testConfigs = testConfigs;
	}

	public String getCompositeServiceFlag() {
		return compositeServiceFlag;
	}

	public void setCompositeServiceFlag(String compositeServiceFlag) {
		this.compositeServiceFlag = compositeServiceFlag;
	}

	

}
