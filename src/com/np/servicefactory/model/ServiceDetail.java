package com.np.servicefactory.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;


public class ServiceDetail implements Serializable{

	private static final long serialVersionUID = 8707444036973611206L;
	
	private String serviceName;
	private String serviceTargetNameSpace;
	private String serviceStatus;
	private String portName;
	private String businessDomain;
	
	private List<OperationDetail> operationDetails;
	private List<FrontSideHandler> frontSideHandlers;
	private ServiceEnvironment environment;
	private List<String> operationNameList;
	
	
	private List<String> wsdlFiles;
	private List<String> xsdFiles;	

	private Set<String> serviceDependentXSDFiles;
	private String rootWsdlName;

	private ServiceFactoryFolderStructure serviceFolder;
	
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceTargetNameSpace() {
		return serviceTargetNameSpace;
	}
	public void setServiceTargetNameSpace(String serviceTargetNameSpace) {
		this.serviceTargetNameSpace = serviceTargetNameSpace;
	}
	public String getServiceStatus() {
		return serviceStatus;
	}
	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}
	public List<OperationDetail> getOperationDetails() {
		return operationDetails;
	}
	public void setOperationDetails(List<OperationDetail> operationDetails) {
		this.operationDetails = operationDetails;
	}
	public List<FrontSideHandler> getFrontSideHandlers() {
		return frontSideHandlers;
	}
	public void setFrontSideHandlers(List<FrontSideHandler> frontSideHandlers) {
		this.frontSideHandlers = frontSideHandlers;
	}
	public List<String> getWsdlFiles() {
		return wsdlFiles;
	}
	public void setWsdlFiles(List<String> wsdlFiles) {
		this.wsdlFiles = wsdlFiles;
	}
	public List<String> getXsdFiles() {
		return xsdFiles;
	}
	public void setXsdFiles(List<String> xsdFiles) {
		this.xsdFiles = xsdFiles;
	}
	public String getRootWsdlName() {
		return rootWsdlName;
	}
	public void setRootWsdlName(String rootWsdlName) {
		this.rootWsdlName = rootWsdlName;
	}
	
	public ServiceEnvironment getEnvironment() {
		return environment;
	}
	public void setEnvironment(ServiceEnvironment environment) {
		this.environment = environment;
	}
	public ServiceFactoryFolderStructure getServiceFolder() {
		return serviceFolder;
	}
	public void setServiceFolder(ServiceFactoryFolderStructure serviceFolder) {
		this.serviceFolder = serviceFolder;
	}
	public List<String> getOperationNameList() {
		return operationNameList;
	}
	public void setOperationNameList(List<String> operationNameList) {
		this.operationNameList = operationNameList;
	}
	public String getPortName() {
		return portName;
	}
	public void setPortName(String portName) {
		this.portName = portName;
	}
	public Set<String> getServiceDependentXSDFiles() {
		return serviceDependentXSDFiles;
	}
	public void setServiceDependentXSDFiles(Set<String> serviceDependentXSDFiles) {
		this.serviceDependentXSDFiles = serviceDependentXSDFiles;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getBusinessDomain() {
		return businessDomain;
	}
	public void setBusinessDomain(String businessDomain) {
		this.businessDomain = businessDomain;
	}
	
	
}
