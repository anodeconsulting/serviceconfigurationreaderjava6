package com.np.servicefactory.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class TestConfiguration implements Serializable  {
	
	private static final long serialVersionUID = 2621622798467295646L;
	
	String sampleSoapRequestFileName;
	String responseFileName;
	String latency;
	String routingRule;
	
	public String getSampleSoapRequestFileName() {
		return sampleSoapRequestFileName;
	}
	public void setSampleSoapRequestFileName(String sampleSoapRequestFileName) {
		this.sampleSoapRequestFileName = sampleSoapRequestFileName;
	}
	
	public String getResponseFileName() {
		return responseFileName;
	}
	public void setResponseFileName(String responseFileName) {
		this.responseFileName = responseFileName;
	}
	public String getLatency() {
		return latency;
	}
	public void setLatency(String latency) {
		this.latency = latency;
	}
	public String getRoutingRule() {
		return routingRule;
	}
	public void setRoutingRule(String routingRule) {
		this.routingRule = routingRule;
	}
	
}
