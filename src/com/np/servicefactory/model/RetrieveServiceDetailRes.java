package com.np.servicefactory.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true) 
public class RetrieveServiceDetailRes {
	List<OperationDetail> operationDetails;
	List<String> environments;
	
	public List<OperationDetail> getOperationDetails() {
		return operationDetails;
	}
	public void setOperationDetails(List<OperationDetail> operationDetails) {
		this.operationDetails = operationDetails;
	}
	public List<String> getEnvironments() {
		return environments;
	}
	public void setEnvironments(List<String> environments) {
		this.environments = environments;
	}
}
