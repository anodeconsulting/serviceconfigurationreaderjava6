/*
 *  Retrieve detail backend information from use web interface 
 * 
 */
package com.np.servicefactory.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Wang
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true) 
public class ServiceProvider implements Serializable {
	
	private static final long serialVersionUID = 8339920411551984076L;
	
	public String inputTransformation;
	public String outputTransformation;

	public String routingRule;	
	public String backEndUrlKey;	
	public String sqlInputArgumentsTransFileName;
	public String sqlResultsetTransformFileName;
	public String sqlDatasourceNameKey;
	public String sqlStatementKey;
	
	

	public String getSqlStatementKey() {
		return sqlStatementKey;
	}
	public void setSqlStatementKey(String sqlStatementKey) {
		this.sqlStatementKey = sqlStatementKey;
	}

	public String getInputTransformation() {
		return inputTransformation;
	}
	public void setInputTransformation(String inputTransformation) {
		this.inputTransformation = inputTransformation;
	}
	public String getOutputTransformation() {
		return outputTransformation;
	}
	public void setOutputTransformation(String outputTransformation) {
		this.outputTransformation = outputTransformation;
	}
	
	public String getRoutingRule() {
		return routingRule;
	}
	public void setRoutingRule(String routingRule) {
		this.routingRule = routingRule;
	}	
	
	public String getSqlInputArgumentsTransFileName() {
		return sqlInputArgumentsTransFileName;
	}
	public void setSqlInputArgumentsTransFileName(
			String sqlInputArgumentsTransFileName) {
		this.sqlInputArgumentsTransFileName = sqlInputArgumentsTransFileName;
	}
	public String getSqlResultsetTransformFileName() {
		return sqlResultsetTransformFileName;
	}
	public void setSqlResultsetTransformFileName(
			String sqlResultsetTransformFileName) {
		this.sqlResultsetTransformFileName = sqlResultsetTransformFileName;
	}
	
	public String getBackEndUrlKey() {
		return backEndUrlKey;
	}
	public void setBackEndUrlKey(String backEndUrlKey) {
		this.backEndUrlKey = backEndUrlKey;
	}
	public String getSqlDatasourceNameKey() {
		return sqlDatasourceNameKey;
	}
	public void setSqlDatasourceNameKey(String sqlDatasourceNameKey) {
		this.sqlDatasourceNameKey = sqlDatasourceNameKey;
	}
	
}
