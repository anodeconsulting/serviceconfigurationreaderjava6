function collectEnviromentData() {
	var enviromentProperties = {
			projectName: "",
			environments: [],
			domainName: "",
			repositoryUrl: "",
			shouldRestDomain: "",
			shouldFlushCache: ""
	}
	 
	var projectName = document.getElementById("project_Name_id");
	enviromentProperties.projectName = projectName.value;
	
	var domainName = document.getElementById("domain_name_id");
	enviromentProperties.domainName = domainName.value;	
	
	var repositoryUrl = document.getElementById("repository_url_id");
	enviromentProperties.repositoryUrl = repositoryUrl.value;
	
    var shouldFlushCaches = document.getElementsByName('flush_cache');
	
	for(var i = 0; i < shouldFlushCaches.length; i++){
	    if(shouldFlushCaches[i].checked){
	    	enviromentProperties.shouldFlushCache = shouldFlushCaches[i].value;
	    }
	}	
	var environments = global_var.environments;
	
	for(var i = 0 ; i < environments.length; i++) {
		var checkBox = document.getElementById(environments[i] + "_checkbox");
		if(checkBox.checked) {
			var environmentSpecificProp = {};
			collectEnvironmentSpecificProps(environmentSpecificProp, environments[i]);
			enviromentProperties.environments.push(environmentSpecificProp);
		} 		
	}
	
	return enviromentProperties;
}

function collectEnvironmentSpecificProps(environmentSpecificProp, idPrefix) {
	var environmentID = idPrefix + "_environment_name";
	var environmentNameEle = document.getElementById(environmentID);
	environmentSpecificProp.environmentName = environmentNameEle.value;
	environmentSpecificProp.environmentType = idPrefix;
	environmentSpecificProp.backEndUrls=[];
	environmentSpecificProp.sqlDataSourceNames = [];
	environmentSpecificProp.sqlStatements =[];
	
	var operationDetails = global_var.operationDetails;
	for(var i = 0; i < operationDetails.length; i++) {
		var operationDetail = operationDetails[i];
		var pattern = operationDetail.pattern;
		var providers = operationDetail.providers;
		if(pattern == 'odbc') {
			collectOdbcData(providers, environmentSpecificProp, idPrefix);
		} else {
			collectBackEndUrls(providers, environmentSpecificProp, idPrefix);
		}
	}
}

function collectBackEndUrls(providers, environmentSpecificProp, idPrefix) {
	for (var j = 0 ; j < providers.length; j++) {
		var backEndUrl = {};
		var provider = providers[j];
		backEndUrl.key =  provider.backEndUrlKey;
		
		var id = idPrefix+"_"+ j + "backEndUrl";
		var backendUrlEle = document.getElementById(id);
		backEndUrl.value = backendUrlEle.value;
		environmentSpecificProp.backEndUrls.push(backEndUrl);
	}
}

function collectOdbcData(providers, environmentSpecificProp, idPrefix) {
	for (var j = 0 ; j < providers.length; j++) {
		var datasource = {};
		var sqlStatment={};
		var provider = providers[j];
		datasource.key =  provider.sqlDatasourceNameKey;
		
		var datasourceID = idPrefix+"_"+ j + "_sql_datasource_name";
		var datasourceEle = document.getElementById(datasourceID);
		
		datasource.value = datasourceEle.value;
		environmentSpecificProp.sqlDataSourceNames.push(datasource);
		
		sqlStatment.key=provider.sqlStatementKey;
		
		var sqlStatementId = idPrefix+"_"+ j + "_sql_statement";
		var sqlStatementEle = document.getElementById(sqlStatementId);
		sqlStatment.value = sqlStatementEle.value;
		environmentSpecificProp.sqlStatements.push(sqlStatment);
		
	}
}

YUI().use('event', function (Y) {  
	var nextButton = "next_Button";
	Y.one("#"+nextButton).on('click', handleClick);
	function handleClick(e) {	
		 
        YUI().use('io', "json-parse", "json-stringify", function (Y) {
        	var cfg,
            request;
        	var enviromentData = collectEnviromentData();
        	cfg = {
        			method: 'POST',
        	        sync: true,
        	        data:  Y.JSON.stringify(enviromentData),
        	        headers: {
        	        	'Accept': 'application/json',
        	        	'Content-Type': 'application/json'
        	          }
        	      
        	    };
        	
        	function onSuccess(transactionid, response) {
        		  try {
                      var messages = Y.JSON.parse(response.responseText);
                      window.location.href="/sf_ide/download"
                      
                  }
                  catch (e) {
                      alert("JSON Parse failed!");
                      return;
                  }
                  
              
        	}

        		// Subscribe to "io.success".
        	Y.on('io:success', onSuccess, Y, true);
        	request = Y.io("/sf_ide/processEnvironmentProperties", cfg);
        });
		
	}
});


YUI().use('event-base', function (Y) {
  

    Y.on('domready', function () {
    	YUI().use('io', "json-parse",  function (Y) {
    		var cfg,
    		request;
    		
    		cfg = {
    				method: 'GET',
    				sync: true,    				
    				headers: {                              
    					'Accept': 'application/json',
    					'Content-Type': 'application/json'
    				},
    				
    		};
    	
    		function onSuccess(transactionid, response) {    		
                  var response = Y.JSON.parse(response.responseText);
                  global_var.operationDetails = response.operationDetails;
                  global_var.environments = response.environments;
                  createEnvironmentTypeSec(response.environments);
                 
            }

    		// Subscribe to "io.success".
    		Y.on('io:success', onSuccess, Y, true);
    	    request = Y.io("/sf_ide/retrieveOperationDetails", cfg);
    	});
    });
});

var global_var = {
		operationDetails: [],
        environments: []
};

function createEnvironmentTypeSec(environmentTypes) {
	var environmentTypeSec = document.getElementById("environment_type_sec");
	var tb2  = document.createElement('table');
	tb2.style="width: 100%";
	var tr = tb2.insertRow();
	for(var i = 0 ; i < environmentTypes.length; i++) {
		var td = tr.insertCell();
		td.appendChild(document.createTextNode(environmentTypes[i] + ' Environment Configuration '));
		var checkbox = document.createElement("input");
		checkbox.type = "checkbox";            
    	if(i == 0) {
    		checkbox.checked = true;    		
    	}
    	checkbox.setAttribute("id", environmentTypes[i]+"_checkbox")
    	td.appendChild(checkbox);    	
	}
	
	environmentTypeSec.appendChild(tb2);
	
	for(var i = 0 ; i < environmentTypes.length; i++) {
		 registerCheckBoxEnvent(environmentTypes[i] );
	}
	
	createEnvironmentContentSec(environmentTypes[0]);
}

function createServiceProviderSection(providerSpecificSec, environmentType) {
	var sec_id = environmentType + "_provider_specific_sec";
	
	var operationDetails = global_var.operationDetails;
	for(var i = 0; i < operationDetails.length; i++) {
		var operationDetail = operationDetails[i];
		var operationPattern = operationDetail.pattern;
		var providers = operationDetail.providers;
		if(operationPattern == 'odbc') {
			createOdbcSec(providers, providerSpecificSec, environmentType);
		} else {
			createBackendURLSec(providers, providerSpecificSec, environmentType);
		}
	}
}

function createOdbcSec(providers, providerSpecificSec, environmentType) {
	for (var j = 0 ; j < providers.length; j++) {
		var provider = providers[j];
		
		var tbl  = document.createElement('table');
		 for(var k = 0; k < 3; k++){
			 var tr = tbl.insertRow();
			 for (var l = 0 ; l < 2 ; l++) {
				 var td = tr.insertCell();
				 if(k == 0 && l == 0) {
					 var textNodeDiv = document.createElement("div");
					 var textNode = document.createTextNode(provider.sqlDatasourceNameKey);
					 textNodeDiv.appendChild(textNode);
				 	 td.appendChild(textNodeDiv);
				 }
				 				 
				 if(k == 0 && l == 1) {
					 var input = document.createElement("input");
					 input.type = "text";
					 input.setAttribute("id", environmentType+"_"+ j + "_sql_datasource_name");
					 input.style = "width: 50.25em";
		            	td.appendChild(input);
				 }
				 
				 if(k == 1 && l == 0) {
					 var textNodeDiv = document.createElement("div");
					 var textNode = document.createTextNode(provider.sqlStatementKey);
					 textNodeDiv.appendChild(textNode);
				 	 td.appendChild(textNodeDiv);
				 }
				 
				 if(k == 1 && l == 1) {
					 var input = document.createElement("input");
					 input.type = "text";
					 input.setAttribute("id", environmentType+"_"+ j + "_sql_statement");
					 input.style = "width: 50.25em";
		            	td.appendChild(input);
				 }
				 if(k == 2 && l == 0) {
					 td.appendChild(document.createTextNode("Routing Rule"));
				 }
				 if(k == 2 && l == 1) {
					 td.appendChild(document.createTextNode(provider.routingRule));
				 }	 
				 
			 }
		 }
		 providerSpecificSec.appendChild(tbl);	
		}
}

function createBackendURLSec(providers, providerSpecificSec, environmentType) {
	for (var j = 0 ; j < providers.length; j++) {
		var provider = providers[j];
		
		var tbl  = document.createElement('table');
		 for(var k = 0; k < 2; k++){
			 var tr = tbl.insertRow();
			 for (var l = 0 ; l < 2 ; l++) {
				 var td = tr.insertCell();
				 if(k == 0 && l == 0) {
					 var textNodeDiv = document.createElement("div");
					 var textNode = document.createTextNode(provider.backEndUrlKey);
					 textNodeDiv.appendChild(textNode);
				 	 td.appendChild(textNodeDiv);
				 }
				 
				 if(k == 0 && l == 1) {
					 var input = document.createElement("input");
					 input.type = "text";
					 input.setAttribute("id", environmentType+"_"+ j + "backEndUrl");
					 input.style = "width: 50.25em";
		            	td.appendChild(input);
				 }
				 if(k == 1 && l == 0) {
					 td.appendChild(document.createTextNode("Routing Rule"));
				 }
				 if(k == 1 && l == 1) {
					 td.appendChild(document.createTextNode(provider.routingRule));
				 }	 
				 
			 }
		 }
		 providerSpecificSec.appendChild(tbl);	
		}
}

function registerCheckBoxEnvent(enviromentType) {
	
	YUI().use('event', function(Y) {
		
		var check = Y.one("#" + enviromentType + "_checkbox");

		check.on('change', function(e) {
			var environmentTypeSec = document.getElementById(enviromentType + "_sec_id");
			if(environmentTypeSec == null) {
				environmentTypeSec = createEnvironmentContentSec(enviromentType);
			}
			var checkbox = e.target;
			var developerSec = document.getElementById(enviromentType + "_sec_id");
			if (checkbox.get('checked')) {				
				developerSec.style = "display:block";

			} else {
				var developerSec = document.getElementById(enviromentType + "_sec_id");
				if(developerSec != null) { 
					developerSec.style = "display:none";
				}
			}
		});

	});

}

function createEnvironmentContentSec(environmentType) {
	var secDiv = document.createElement("div");
	secDiv.setAttribute("id", environmentType+"_sec_id")
	secDiv.className="content";
	
	var nextDiv = document.createElement("div");
	nextDiv.className="example yui3-skin-sam";
	
	secDiv.appendChild(nextDiv);
	
	
	var titleDiv = document.createElement("div");
	var h2 = document.createElement("h2");
	titleDiv.appendChild(h2);
	
	var txt = document.createTextNode(environmentType + " Environment Configuration");
	h2.appendChild(txt);
	 
	 nextDiv.appendChild(titleDiv);
	 var br = document.createElement("br");
	 nextDiv.appendChild(br);
	 var br = document.createElement("br");
	 nextDiv.appendChild(br);
	var contentDiv = document.createElement("div");
	var tb2  = document.createElement('table');
	tb2.style="width: 100%";
	var tr = tb2.insertRow();
	var td = tr.insertCell();
	td.appendChild(document.createTextNode("Environment Name "));
	var td = tr.insertCell();
	var input = document.createElement("input");
	input.type = "text";            
	input.setAttribute("id", environmentType+"_environment_name");
	input.value=environmentType;
	td.appendChild(input);
	
	contentDiv.appendChild(tb2);
	nextDiv.appendChild(contentDiv);
	var providerSpecificSec = document.createElement("div");
	var providerSpecificSecId = environmentType+"_provider_specific_sec";
	providerSpecificSec.setAttribute("id", providerSpecificSecId);
	nextDiv.appendChild(providerSpecificSec);
	createServiceProviderSection(providerSpecificSec, environmentType);
	var detailSec = document.getElementById("environment_type_sec");
	detailSec.appendChild(secDiv);
	return secDiv;
}


