
var globalVar = {
		operationObjs: {},
        frontSideHandlerCount: 1,
        operationRecord: [],
		selectedOperation:[]
} 

YUI({filter:"raw"}).use("uploader", function(Y) {
 
   if (Y.Uploader.TYPE != "none" && !Y.UA.ios) {
       var uploader = new Y.Uploader({width: "250px",
                                      height: "35px",
                                      multipleFiles: true,
                                      postVarsPerFile: {service: "test"},
                                      swfURL: "flashuploader.swf?t=" + Math.random(),
                                      uploadURL: "/sf_ide/uploadWSDL",
                                      simLimit: 2,
                                      withCredentials: false
                                     });
       var uploadDone = false;

       uploader.render("#selectFilesButtonContainer");

       uploader.after("fileselect", function (event) {

          var fileList = event.fileList;
          var fileTable = Y.one("#filenames tbody");
          if (fileList.length > 0 && Y.one("#nofiles")) {
            Y.one("#nofiles").remove();
          }

          if (uploadDone) {
            uploadDone = false;
            fileTable.setHTML("");
          }
         

          Y.each(fileList, function (fileInstance) {
              fileTable.append("<tr id='" + fileInstance.get("id") + "_row" + "'>" +
                                    "<td class='filename'>" + fileInstance.get("name") + "</td>" +
                                    "<td class='filesize'>" + fileInstance.get("size") + "</td>" +
                                    "<td class='percentdone'>Hasn't started yet</td>");
                             });
       });

       uploader.on("uploadprogress", function (event) {
            var fileRow = Y.one("#" + event.file.get("id") + "_row");
                fileRow.one(".percentdone").set("text", event.percentLoaded + "%");
       });

       uploader.on("uploadstart", function (event) {
            uploader.set("enabled", false);
            Y.one("#uploadFilesButton").addClass("yui3-button-disabled");
            Y.one("#uploadFilesButton").detach("click");
       });

       uploader.on("uploadcomplete", function (event) {
            var fileRow = Y.one("#" + event.file.get("id") + "_row");
                fileRow.one(".percentdone").set("text", "Finished!");
                
                YUI().use('io', "json-parse", "json-stringify", function (Y) {
                	var wsdlList = Y.JSON.parse(event.data);
                	var rootWsdlEntryDiv = document.getElementById("root_wsdl_entry") ;
                	rootWsdlEntryDiv.style.display = "block";
                	var select = document.getElementById("wsdlList");
                   	select.disabled = false;
                	for (var index in wsdlList) {
                		var option = document.createElement('option');
                		option.innerHTML = wsdlList[index];
                		option.value = wsdlList[index];
                		select.appendChild(option)
                	}
                });
                
       });

       uploader.on("totaluploadprogress", function (event) {
                Y.one("#overallProgress").setHTML("Total uploaded: <strong>" + event.percentLoaded + "%" + "</strong>");
       });

       uploader.on("alluploadscomplete", function (event) {
                     uploader.set("enabled", true);
                     uploader.set("fileList", []);
                     Y.one("#uploadFilesButton").removeClass("yui3-button-disabled");
                     Y.one("#uploadFilesButton").on("click", function () {                         
                        	 uploader.uploadAll();
                                                  
                          
                     });
                     Y.one("#overallProgress").set("text", "Uploads complete!");
                     uploadDone = true;
       });

       Y.one("#uploadFilesButton").on("click", function () {
         if (!uploadDone && uploader.get("fileList").length > 0) {
        	 uploader.uploadAll();       
         }
       });
   }
   else {
       Y.one("#uploaderContainer").set("text", "We are sorry, but to use the uploader, you either need a browser that support HTML5 or have the Flash player installed on your computer.");
   }


});


YUI().use('event', function(Y) {
	Y.one("#" + "wsdlList").on("change", function() {
		// 'this' points to dropdown node
		// so it's value is a value of selected option. Assume you keep new url
		// there
		var optionValue = this.get('value');
		if (optionValue === "0") {
			return;
		} else {
			processWsdl(optionValue);
			var wsdlElement = document.getElementById("wsdlList");
			wsdlElement.disabled = true;
		}			

	});
    }

);
	
function processWsdl(selectedWsdlName) {	

	YUI().use('io', "json-parse", "json-stringify", function (Y) {
		var cfg,
		request;
		var rootWsdlNameEle = document.getElementById("root_wsdl_name");
		var rootWsdlName = rootWsdlNameEle
		cfg = {
				method: 'POST',
				sync: true,
				data:  Y.JSON.stringify({rootWsdlName: selectedWsdlName}),
				headers: {                              
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				arguments: { 'foo' : 'bar' }
		};
	
		function onSuccess(transactionid, response, arguments) {
			try {
              var messages = Y.JSON.parse(response.responseText);
               createOptTableRecords(messages["operationNameList"]);
			}
          catch (e) {
              alert("JSON Parse failed!");
              return;
          }
		}

		// Subscribe to "io.success".
		Y.on('io:success', onSuccess, Y, true);
	    request = Y.io("/sf_ide/processUploadWsdl", cfg);
	});
}


function tableCreate(operationName, operationID){
	var operationDetail = document.getElementById("operationDetail");
	
	
	
	var opeationDiv = document.createElement("div");
	
	opeationDiv.style= "border-radius: 1px; box-shadow: 0 0 3px #000000; padding: 1em;";
	
	
	var titleDiv = document.createElement("div");
	titleDiv.innerHTML="<h3>"+ operationID+ " Configuration</h3>";
	opeationDiv.appendChild(titleDiv);
	

	 
	 var br = document.createElement("br");
	 opeationDiv.appendChild(br);
	 
	opeationDiv.setAttribute("id", operationID);
	var divSection = document.createElement("div");
	divSection.setAttribute("id", operationID + "_operationTitle");
	
    tbl  = document.createElement('table');
   
    var toggleButtonID = operationID+"_toggleButton";
    var toggleButtonEle = document.createElement("button");	
    toggleButtonEle.setAttribute("id", toggleButtonID);
    toggleButtonEle.innerHTML = "Provider Detail Toggle Button";
    
    var patternSelectionId = operationID + "_patternID";
    for(var i = 0; i < 2; i++){
        var tr = tbl.insertRow();
        for(var j = 0; j < 3; j++){      
        	
            var td = tr.insertCell();
            if(j==0) {
            	td.style.width = '30%';
            }
            if( i == 0 && j == 0) {
            	td.appendChild(document.createTextNode('Operation  Name: '));
            }
            
            if( i == 0 && j == 1) {
            	td.appendChild(document.createTextNode(operationName));
            }
            
            if(i == 1 && j == 0) {
            	td.appendChild(document.createTextNode('Pattern'));
            }
            
            if(i == 1 && j == 1) {            	
            	td.innerHTML = "<select id='" + patternSelectionId + "'> <option value='" + 
            	operationID + "_binary'>"+"Binary</option><option value='" + operationID+ "_odbc'>Odbc</option>" + "<option value='" + operationID+ "_versiongateway'>Version Gateway</option>	</select>";
            }
            if( i == 0 && j == 2) {          
            	td.appendChild(toggleButtonEle);
            }
               
        }
    }
   
    divSection.appendChild(tbl);  
    var providerSection = createProviderDetail(operationID, true);
   
    opeationDiv.appendChild(divSection);    
    var br = document.createElement("br");
    opeationDiv.appendChild(providerSection);
    var testDetailSec = createTestDetail(operationID, true);
    opeationDiv.appendChild(br);
    opeationDiv.appendChild(testDetailSec);
    
    var br = document.createElement("br");
 
    operationDetail.appendChild(opeationDiv);
    operationDetail.appendChild(br);
    operationDetail.appendChild(br);
    
    registerEventForProviderDetailSec(operationID);
    registerEventForTestDetailSec(operationID);
    registerFileLoaderForBinaryProvider(operationID);
    registerFileLoaderForTestConfig(operationID);
    YUI().use('node', function(Y) {
    	Y.one("#" + toggleButtonID).on('click', function(e) {
            var buttonID = e.currentTarget.get('id'),
            node = Y.one('#' + operationID + "_detailSecID");
          
            if(buttonID === toggleButtonID) {
                node.toggleView();
            }
            
            node = Y.one('#' + operationID + "_test_SecID");
            
            if(buttonID === toggleButtonID) {
                node.toggleView();
            }
        }, document, 'button');
    });
    YUI().use('event', function (Y) {
    	Y.one("#" + patternSelectionId).on("change", function() {
        // 'this' points to dropdown node
        // so it's value is a value of selected option. Assume you keep new url
		// there
    		var optionValue = this.get('value'); 
    		var providerDetailDiv = Y.one('#' + operationID + "_detailSecID");
    		providerDetailDiv.remove();
    		globalVar.operationObjs[operationID+"_providerCount"] = 0;
        
    		var binarySectionId = operationID+"_binary";
    		var versiongatewayId = operationID + "_versiongateway";
    		var providerSection = null;
    		if(optionValue == binarySectionId || optionValue == versiongatewayId) {        	
    			providerSection = createProviderDetail(operationID, true);
    		} else {
    			providerSection = createProviderDetail(operationID, false);
    		}
    		var operationDev =  Y.one('#' + operationID);
    		var testDetailDiv = Y.one('#' + operationID + "_test_SecID");
    		var br = document.createElement("br");
    		operationDev.insertBefore(br, testDetailDiv);
    		operationDev.insertBefore(providerSection, br);
    		registerEventForProviderDetailSec(operationID);    		
    		if(optionValue == binarySectionId || optionValue == versiongatewayId) {
    			registerFileLoaderForBinaryProvider(operationID);
    		} else {
    			registerFileLoaderForOdbcProvider(operationID);
    		}
    	});
    
    });   
  
};

YUI().use('node', function(Y) {
	Y.one("#" + "fsh_toggle_btn").on('click', function(e) {
        var buttonID = e.currentTarget.get('id'),
        node = Y.one('#' + "frh_detail_sec");
      
        if(buttonID === "fsh_toggle_btn") {
            node.toggleView();
        }
    }, document, 'button');
});



function createProviderDetail(operationID, isForBinary) {
	 var providerSection = document.createElement("div");
		providerSection.setAttribute("id", operationID + "_detailSecID");
		providerSection.style= "border-radius: 1px; box-shadow: 0 0 3px #000000; padding: 1em;";
		
	    var providerDetailSection;
	    
	    if(isForBinary) {
	    	providerDetailSection = createProviderDetailSecForBinary(operationID);
	    } else {
	    	providerDetailSection = createProviderDetailSecForOdbc(operationID);
	    }
	    
	    providerSection.appendChild(providerDetailSection);
	    
	    var addButtonEle = document.createElement("button");
	    var addButtonID = operationID+"_addbutton";
	    addButtonEle.setAttribute("id", addButtonID);
	    addButtonEle.innerHTML = "Add";
	    
	    var removeButtonEle = document.createElement("button");
	    var removeButtonID = operationID+"_removeButtonID";
	    removeButtonEle.setAttribute("id", removeButtonID);
	    removeButtonEle.innerHTML = "Remove";
	    providerSection.appendChild(addButtonEle);
	    providerSection.appendChild(removeButtonEle);
	       
	    return providerSection;
}

function createTestDetail(operationID, isForBinary) {
	var testSection = document.createElement("div");
	testSection.setAttribute("id", operationID + "_test_SecID");
	testSection.style = "border-radius: 1px; box-shadow: 0 0 3px #000000; padding: 1em;";

	testDetailSection = createTestDetailSec(operationID,
			isForBinary);

	testSection.appendChild(testDetailSection);

	var addButtonEle = document.createElement("button");
	var addButtonID = operationID + "_test_addbutton";
	addButtonEle.setAttribute("id", addButtonID);
	addButtonEle.innerHTML = "Add";

	var removeButtonEle = document.createElement("button");
	var removeButtonID = operationID + "_test_removeButtonID";
	removeButtonEle.setAttribute("id", removeButtonID);
	removeButtonEle.innerHTML = "Remove";
	testSection.appendChild(addButtonEle);
	testSection.appendChild(removeButtonEle);

	return testSection;
}

function createTestDetailSec(operationID, isForBinary) {
	if(!globalVar.operationObjs[operationID+"_testDetailCount"]) {
	    	globalVar.operationObjs[operationID+"_testDetailCount"] = 0;
	}
	
	var testDetail = document.createElement("div");

	var title = document.createTextNode("Test Configuration Detail");
	testDetail.appendChild(title);
	var idPrefix = operationID +"_" + (globalVar.operationObjs[operationID+"_testDetailCount"]+1);
	
    tb2  = document.createElement('table');
    for(var i = 0; i < 4; i++){
        var tr = tb2.insertRow();
        for(var j = 0; j < 2; j++){           
            var td = tr.insertCell();
            if(j==0) {
            	td.style.width = '30%';
            }
            if( i == 0 && j == 0) {
            	td.appendChild(document.createTextNode('Upload Test Request File: '));
            }
            
            if( i == 0 && j == 1) {     
            	var uploadDiv = createUploadSection(idPrefix, "sampleReqFile")
           	 	td.appendChild(uploadDiv);
            }
            
            if(i == 1 && j == 0) {
            	td.appendChild(document.createTextNode('Upload Test Reponse File: '));
            }
            
            
            if(i == 1 && j == 1) {
            	var uploadDiv = createUploadSection(idPrefix, "respFile")
            	td.appendChild(uploadDiv);
            }
            
            
            if(i == 2 && j == 0) {
            	td.appendChild(document.createTextNode('Latency: '));
            }
            
            if(i == 2 && j == 1) {
            	var input = document.createElement("input");
            	input.type = "text";            	
            	input.setAttribute("id", idPrefix+"_latency");
            	input.value="0";
            	td.appendChild(input);
            }
            
            if(i == 3 && j == 0) {
            	td.appendChild(document.createTextNode('Routing Rule: '));
            }
            
            if(i == 3 && j == 1) {
            	var input = document.createElement("input");
            	input.type = "text";
            	input.style = "width: 50.25em";
            	input.setAttribute("id", idPrefix+"_routingRule")
            	td.appendChild(input);
            }
        }
    }
    testDetail.appendChild(tb2);
   
    
    testDetail.setAttribute("id",  operationID +"_testDetailSection_" + (globalVar.operationObjs[operationID+"_testDetailCount"]+1));
    globalVar.operationObjs[operationID+"_testDetailCount"] = globalVar.operationObjs[operationID+"_testDetailCount"] + 1;
    
 
    return testDetail;
}


function registerEventForProviderDetailSec(operationID) {
	 var patternSelectionId = operationID + "_patternID";
	 YUI().use('event', function (Y) {  
	    	var addButtonID = operationID+"_addbutton";
	    	Y.one("#"+addButtonID).on('click', handleClick);
	    	function handleClick(e) {
	    		var pattenSelctionEle =  Y.one("#" + patternSelectionId);
	    		var patternValue = pattenSelctionEle.get("value");
	    		var binarySectionId = operationID+"_binary";
	    		var versiongatewayId = operationID+"_versiongateway";
	    		
	    		var providerDetailSec = null;
	    		if(patternValue == binarySectionId || patternValue == versiongatewayId) {
	    			providerDetailSec = createProviderDetailSecForBinary(operationID);
	    		} else {
	    			providerDetailSec = createProviderDetailSecForOdbc(operationID);
	    		}
	    		var binaryDetail = document.getElementById(operationID+"_detailSecID");
	    		var buttonElement = document.getElementById(addButtonID);
	    		binaryDetail.insertBefore(providerDetailSec, buttonElement);
	    		if(patternValue == binarySectionId || patternValue == versiongatewayId ) {
	    			registerFileLoaderForBinaryProvider(operationID);
	    		} else {
	    			registerFileLoaderForOdbcProvider(operationID);
	    		}
	    	}
	    	});	
	    
	    YUI().use('event', function (Y) {  
	    	var removeButtonID = operationID+"_removeButtonID";
	    	Y.one("#"+ removeButtonID).on('click', handleClick);
	    	function handleClick(e) {    		
	    		var divID = operationID +"_providerDetailSection_" + (globalVar.operationObjs[operationID+"_providerCount"]);
	    		var divIDSec = document.getElementById(divID);    		
	    	    divIDSec.remove();
	    	    globalVar.operationObjs[operationID+"_providerCount"] = globalVar.operationObjs[operationID+"_providerCount"] - 1;
	    	}
	    	});	  
	   
}


function registerEventForTestDetailSec(operationID) {
	 YUI().use('event', function (Y) {  
	    	var addButtonID = operationID+"_test_addbutton";
	    	Y.one("#"+addButtonID).on('click', handleClick);
	    	function handleClick(e) {	
	    		var testDetailSec = createTestDetailSec(operationID, true);
	    		
	    		var testSec = document.getElementById(operationID+"_test_SecID");
	    		var buttonElement = document.getElementById(addButtonID);
	    		testSec.insertBefore(testDetailSec, buttonElement);	    	
	    		registerFileLoaderForTestConfig(operationID);
	    		
	    	}
	    	});	
	    
	    YUI().use('event', function (Y) {  
	    	var removeButtonID = operationID+"_test_removeButtonID";
	    	Y.one("#"+ removeButtonID).on('click', handleClick);
	    	function handleClick(e) {    		
	    		var divID = operationID +"_testDetailSection_" + (globalVar.operationObjs[operationID+"_testDetailCount"]);
	    		var divIDSec = document.getElementById(divID);    		
	    	    divIDSec.remove();
	    	    globalVar.operationObjs[operationID+"_testDetailCount"] = globalVar.operationObjs[operationID+"_testDetailCount"] - 1;
	    	}
	    	});	  
	   
}

function registerFileLoaderForTestConfig(operationID) {
	 YUI({filter:'raw'}).use('uploader', function (Y) {  
		 var idPrefix = operationID +"_" + (globalVar.operationObjs[operationID+"_testDetailCount"]);
      	var uploader_sampleReqFile = new Y.Uploader({ 
      		width: "100px",
              height: "35px",
      		multipleFiles: true,
              swfURL: "flashuploader.swf?t=" + Math.random(),
              uploadURL: "/sf_ide/uploadSampleReqFile",
              simLimit: 2,
              withCredentials: false});
      	
      	uploader_sampleReqFile.render("#" + idPrefix+"_sampleReqFile" +"_selectFileButtonContainer");
        Y.one("#" + idPrefix+"_" + "sampleReqFile" +"_uploadFilesButton").on("click", function () {
            if (uploader_sampleReqFile.get("fileList").length > 0) {            
            	 var fileList = uploader_sampleReqFile.get("fileList");
            	 var lastFile = fileList[fileList.length -1];
            	 uploader_sampleReqFile.upload(lastFile);
            	 fileList.length = 0;  
            	
            }
          });
        var sampleReqFile_containerID = idPrefix+"_sampleReqFile"+"_msgContainer";
        uploader_sampleReqFile.after("fileselect", function (event) {
       	 
            var fileList = event.fileList;
        
            var msgContainer = document.getElementById(sampleReqFile_containerID);
            Y.each(fileList, function (fileInstance) {
            	 while( msgContainer.hasChildNodes() ){
                 	msgContainer.removeChild(msgContainer.lastChild);
                 }
           	 var textNode = document.createTextNode(fileInstance.get("name"));
          
           	 msgContainer.appendChild(textNode);
            });
         });
        
        uploader_sampleReqFile.on("uploadcomplete", function (event) {
            var msgContainer = Y.one("#" + sampleReqFile_containerID);
            
            var textNode = document.createTextNode(" uploaded!");
       	   msgContainer.appendChild(textNode);
       });
        
     	var uploader_respFile = new Y.Uploader({ 
      		width: "100px",
              height: "35px",
      		multipleFiles: true,
              swfURL: "flashuploader.swf?t=" + Math.random(),
              uploadURL: "/sf_ide/uploadTestResFile",
              simLimit: 2,
              withCredentials: false});
      	
      	uploader_respFile.render("#" + idPrefix+"_respFile" +"_selectFileButtonContainer");
        Y.one("#" + idPrefix+"_" + "respFile" +"_uploadFilesButton").on("click", function () {
            if (uploader_respFile.get("fileList").length > 0) {            	
            	 var fileList = uploader_respFile.get("fileList");
            	 var lastFile = fileList[fileList.length -1];
            	 uploader_respFile.upload(lastFile);
            	 fileList.length = 0;  
            }
          });
        
        var respFile_containerID = idPrefix+"_respFile"+"_msgContainer";
        uploader_respFile.after("fileselect", function (event) {
       	 
            var fileList = event.fileList;           
            var msgContainer = document.getElementById(respFile_containerID);
            while( msgContainer.hasChildNodes() ){
             	msgContainer.removeChild(msgContainer.lastChild);
             }
            Y.each(fileList, function (fileInstance) {            	
           	     var textNode = document.createTextNode(fileInstance.get("name"));           	    
           	     msgContainer.appendChild(textNode);
            });
         });
        
        uploader_respFile.on("uploadcomplete", function (event) {
            var msgContainer = Y.one("#" + respFile_containerID);
            
            var textNode = document.createTextNode(" uploaded!");
            
       	    msgContainer.appendChild(textNode);
       });
    
      });
	
};


function registerFileLoaderForBinaryProvider(operationID) {
	var patternID = operationID + "_patternID";
	var patternElement = document.getElementById(patternID);		
	var optionValue = patternElement.value;
	
	var pattern = optionValue.substring(optionValue.indexOf("_") + 1);
	var reqfileUploadUrl = null ;
	var resfileUploadUrl = null ;
	
	if(pattern == "binary") {
		reqfileUploadUrl = "/sf_ide/uploadReqDPA";
		resfileUploadUrl = 	"/sf_ide/uploadResDPA";
	} else {
		reqfileUploadUrl = "/sf_ide/uploadReqXSLTFile";
		resfileUploadUrl = "/sf_ide/uploadResXSLTFile"
	}

	
	 YUI({filter:'raw'}).use('uploader', function (Y) {  
       	var idPrefix = operationID + "_"+globalVar.operationObjs[operationID+"_providerCount"];
       	var uploader_reqTransformFile = new Y.Uploader({ 
       		width: "100px",
               height: "35px",
       		multipleFiles: true,
               swfURL: "flashuploader.swf?t=" + Math.random(),
               uploadURL: reqfileUploadUrl,
               simLimit: 2,
               withCredentials: false});
       	
       	uploader_reqTransformFile.render("#" + idPrefix+"_reqDPA" +"_selectFileButtonContainer");
         Y.one("#" + idPrefix+"_" + "reqDPA" +"_uploadFilesButton").on("click", function () {
             if (uploader_reqTransformFile.get("fileList").length > 0) {
            	 var fileList = uploader_reqTransformFile.get("fileList");
            	 var lastFile = fileList[fileList.length -1];
            	 uploader_reqTransformFile.upload(lastFile);
            	 fileList.length = 0;
             }
           });
         var reqDPA_containerID = idPrefix+"_reqDPA"+"_msgContainer";
         uploader_reqTransformFile.after("fileselect", function (event) {
        	 
             var fileList = event.fileList;
             var msgContainer = document.getElementById(reqDPA_containerID);
             while( msgContainer.hasChildNodes() ){
               	msgContainer.removeChild(msgContainer.lastChild);
               }
             Y.each(fileList, function (fileInstance) {       	
            	
            	 var textNode = document.createTextNode(fileInstance.get("name"));            	
            	 msgContainer.appendChild(textNode);
             });
          });
         
         uploader_reqTransformFile.on("uploadcomplete", function (event) {
        	 var msgContainer = document.getElementById(reqDPA_containerID);
            
             var textNode = document.createTextNode(" uploaded!");
        	 msgContainer.appendChild(textNode);
        });
         
      	var uploader_resTransformFile = new Y.Uploader({ 
       		width: "100px",
               height: "35px",
       		multipleFiles: true,
               swfURL: "flashuploader.swf?t=" + Math.random(),
               uploadURL: resfileUploadUrl,
               simLimit: 2,
               withCredentials: false});
       	
      	uploader_resTransformFile.render("#" + idPrefix+"_resDPA" +"_selectFileButtonContainer");
         Y.one("#" + idPrefix+"_" + "resDPA" +"_uploadFilesButton").on("click", function () {
             if (uploader_reqTransformFile.get("fileList").length > 0) {            	
            	 var fileList = uploader_resTransformFile.get("fileList");
            	 var lastFile = fileList[fileList.length -1];
            	 uploader_resTransformFile.upload(lastFile);
            	 fileList.length = 0;  
             }
           });
         
         var resDPA_containerID = idPrefix+"_resDPA"+"_msgContainer";
         uploader_resTransformFile.after("fileselect", function (event) {
        	 
             var fileList = event.fileList;      
             var msgContainer = document.getElementById(resDPA_containerID);
             while( msgContainer.hasChildNodes() ){
             	msgContainer.removeChild(msgContainer.lastChild);
             }
             
             Y.each(fileList, function (fileInstance) {
            	 var textNode = document.createTextNode(fileInstance.get("name"));            	
            	 msgContainer.appendChild(textNode);
             });
          });
         
         uploader_resTransformFile.on("uploadcomplete", function (event) {
             var msgContainer = Y.one("#" + resDPA_containerID);
            
             var textNode = document.createTextNode(" uploaded!");
        	 msgContainer.appendChild(textNode);
        });
     
       });
	
};

function registerFileLoaderForOdbcProvider(operationID) {
	 YUI({filter:'raw'}).use('uploader', function (Y) {  
      	var idPrefix = operationID + "_"+globalVar.operationObjs[operationID+"_providerCount"];
      	var uploader_SQLInputArgumentTransFile = new Y.Uploader({ 
      		width: "100px",
              height: "35px",
      		multipleFiles: true,
              swfURL: "flashuploader.swf?t=" + Math.random(),
              uploadURL: "/sf_ide/uploadSQLInputArgumentTransFile",
              simLimit: 2,
              withCredentials: false});
      	
      	uploader_SQLInputArgumentTransFile.render("#" + idPrefix+"_SQLInputArgumentTransFile" +"_selectFileButtonContainer");
        Y.one("#" + idPrefix+"_" + "SQLInputArgumentTransFile" +"_uploadFilesButton").on("click", function () {
            if (uploader_SQLInputArgumentTransFile.get("fileList").length > 0) {
           	
           	 	var fileList = uploader_SQLInputArgumentTransFile.get("fileList");
           	 	var lastFile = fileList[fileList.length -1];
           	    uploader_SQLInputArgumentTransFile.upload(lastFile);
           	 	fileList.length = 0;  
            }
          });
        var sqlInputArgumentTransFile_msg_containerID = idPrefix+"_SQLInputArgumentTransFile"+"_msgContainer";
        uploader_SQLInputArgumentTransFile.after("fileselect", function (event) {
       	 
            var fileList = event.fileList;
            
            var msgContainer = document.getElementById(sqlInputArgumentTransFile_msg_containerID);
            while( msgContainer.hasChildNodes() ){
               	msgContainer.removeChild(msgContainer.lastChild);
               }
            Y.each(fileList, function (fileInstance) {
              
           	   var textNode = document.createTextNode(fileInstance.get("name"));
           	  
           	   msgContainer.appendChild(textNode);
            });
         });
        
        uploader_SQLInputArgumentTransFile.on("uploadcomplete", function (event) {   
            var msgContainer = document.getElementById(sqlInputArgumentTransFile_msg_containerID);
            
            var textNode = document.createTextNode(" uploaded!");
       	    msgContainer.appendChild(textNode);
       });
        
     	var uploader_SQLResultTransformFile = new Y.Uploader({ 
      		width: "100px",
              height: "35px",
      		multipleFiles: true,
              swfURL: "flashuploader.swf?t=" + Math.random(),
              uploadURL: "/sf_ide/uploadSQLResultTransformFile",
              simLimit: 2,
              withCredentials: false});
      	
      	uploader_SQLResultTransformFile.render("#" + idPrefix+"_SQLResultTransformFile" +"_selectFileButtonContainer");
        Y.one("#" + idPrefix+"_" + "SQLResultTransformFile" +"_uploadFilesButton").on("click", function () {
            if (uploader_SQLResultTransformFile.get("fileList").length > 0) {            	
            	
            	var fileList = uploader_SQLResultTransformFile.get("fileList");
           	 	var lastFile = fileList[fileList.length -1];
           	     uploader_SQLResultTransformFile.upload(lastFile);
           	 	fileList.length = 0;  
            }
          });
        
        var sqlLResultTransformFile_msg_containerID = idPrefix+"_SQLResultTransformFile"+"_msgContainer";
        uploader_SQLResultTransformFile.after("fileselect", function (event) {
       	 
            var fileList = event.fileList;
          
            var msgContainer = document.getElementById(sqlLResultTransformFile_msg_containerID);
            while( msgContainer.hasChildNodes() ){
             	msgContainer.removeChild(msgContainer.lastChild);
             }
            
            Y.each(fileList, function (fileInstance) {
                
           	     var textNode = document.createTextNode(fileInstance.get("name"));
           	    
           	     msgContainer.appendChild(textNode);
            });
         });
        
        uploader_SQLResultTransformFile.on("uploadcomplete", function (event) {
            var msgContainer = Y.one("#" + sqlLResultTransformFile_msg_containerID);
           
            var textNode = document.createTextNode(" uploaded!");
       	 msgContainer.appendChild(textNode);
       });
    
      });
	
};


function createFrontSideHandlerSec( ) {	
	var id = "fsh_detail_" + (globalVar.frontSideHandlerCount + 1);
	var handlerSec = document.createElement("div");
	handlerSec.setAttribute("id", id);
	var title = document.createTextNode("Front Side Handler Detail");
	handlerSec.appendChild(title);
	tb2  = document.createElement('table');
    for(var i = 0; i < 2; i++) {
        var tr = tb2.insertRow();
        for(var j = 0; j < 2; j++){           
            var td = tr.insertCell();
            
            if( i == 0 && j == 0) {
            	td.appendChild(document.createTextNode('Name: '));
            }
            
            if( i == 0 && j == 1) {
            	var input = document.createElement("input");
            	input.setAttribute("id", id+"_name")
            	input.type = "text";
            	td.appendChild(input);
         
            }
            
            if(i == 1 && j == 0) {
            	td.appendChild(document.createTextNode('Type: '));
            }
            
            
            if(i == 1 && j == 1) {
            	var valueID = (globalVar.frontSideHandlerCount + 1);
            	td.innerHTML = "<select id='" + id + "_type'> <option value='" + 
            	valueID+"_HTTPS'>HTTPS Source Protocol Handler</option><option value='" +valueID +"_HTTP'>HTTP Source Protocol Handler</option>	</select>";
            }
        }
    }
    handlerSec.appendChild(tb2);
	return handlerSec;
}

function createProviderDetailSecForBinary(operationID) {
	if(!globalVar.operationObjs[operationID+"_providerCount"]) {
	    	globalVar.operationObjs[operationID+"_providerCount"] = 0;
	}
	
	var providerDetail = document.createElement("div");

	var title = document.createTextNode("Provider Detail");
	providerDetail.appendChild(title);
	var idPrefix = operationID +"_" + (globalVar.operationObjs[operationID+"_providerCount"]+1);
	
    tb2  = document.createElement('table');
    for(var i = 0; i < 4; i++){
        var tr = tb2.insertRow();
        for(var j = 0; j < 2; j++){           
            var td = tr.insertCell();
            if(j==0) {
            	td.style.width = '30%';
            }
            if( i == 0 && j == 0) {
            	td.appendChild(document.createTextNode('Upload Request Transformation File: '));
            }
            
            if( i == 0 && j == 1) {     
            	var uploadDiv = createUploadSection(idPrefix, "reqDPA")
           	 	td.appendChild(uploadDiv);
            }
            
            if(i == 1 && j == 0) {
            	td.appendChild(document.createTextNode('Upload Reponse Transformation File'));
            }
            
            
            if(i == 1 && j == 1) {
            	var uploadDiv = createUploadSection(idPrefix, "resDPA")
            	td.appendChild(uploadDiv);
            }
            
            
            if(i == 2 && j == 0) {
            	td.appendChild(document.createTextNode('Back End URL Key'));
            }
            
            if(i == 2 && j == 1) { 
            	var input = document.createElement("input");
            	input.setAttribute("id", operationID + "_" +(globalVar.operationObjs[operationID+"_providerCount"]+1) + "_backEndUrl");
            	input.type = "text";
            	input.style = "width: 50.25em";
            	input.value = operationID+"_provider_url_" +(globalVar.operationObjs[operationID+"_providerCount"]+1);
            	input.disabled = true;
            	td.appendChild(input);
            }
            
            if(i == 3 && j == 0) {
            	td.appendChild(document.createTextNode('Routing Rule'));
            }
            
            if(i == 3 && j == 1) {
            	var input = document.createElement("input");
            	input.setAttribute("id", operationID +  "_" +(globalVar.operationObjs[operationID+"_providerCount"]+1) + "_routingRule");
            	input.type = "text";
            	input.style = "width: 50.25em";
            	td.appendChild(input);
            }
        }
    }
    providerDetail.appendChild(tb2);
   
    
    providerDetail.setAttribute("id",  operationID +"_providerDetailSection_" + (globalVar.operationObjs[operationID+"_providerCount"]+1));
    globalVar.operationObjs[operationID+"_providerCount"] = globalVar.operationObjs[operationID+"_providerCount"] + 1;
    
 
    return providerDetail;
}

function createUploadSection(idPrefix, fieldName) {
	var uploadDiv = document.createElement("div");            	
	uploadDiv.setAttribute("id", idPrefix+ "_"+fieldName+"_uploaderContainerID");   
	var  selectFileButtonContainer = document.createElement("div");
	selectFileButtonContainer.setAttribute("id", idPrefix + "_"+fieldName+"_selectFileButtonContainer");
	selectFileButtonContainer.style="display: inline-block;"
	uploadDiv.appendChild(selectFileButtonContainer);
	
	var uploadFilesButtonContainer = document.createElement("div");
	uploadFilesButtonContainer.setAttribute("id", idPrefix+"_"+fieldName+"_uploadFilesButtonContainer");
	uploadFilesButtonContainer.style="display: inline-block; padding: 10px;"
	uploadDiv.appendChild(uploadFilesButtonContainer);
	var uploadButton = document.createElement("button");
	uploadButton.className="yui3-button";
	     uploadButton.setAttribute("id", idPrefix+"_"+fieldName+"_uploadFilesButton");
	 uploadButton.innerHTML = "Upload File";
	 uploadFilesButtonContainer.appendChild(uploadButton);
	 var msgContainer = document.createElement("div");
	 msgContainer.setAttribute("id", idPrefix+"_"+fieldName+"_msgContainer");
	 msgContainer.style="display: inline-block; padding: 10px;"
	 uploadDiv.appendChild(msgContainer);
	 return uploadDiv;
}


function createProviderDetailSecForOdbc(operationID) {
	
	var providerDetail = document.createElement("div");

	var title = document.createTextNode("Provider Detail");
	providerDetail.appendChild(title);
	
	if(!globalVar.operationObjs[operationID+"_providerCount"]) {
    	globalVar.operationObjs[operationID+"_providerCount"] = 0;
    }
    
	var idPrefix = operationID +"_" + (globalVar.operationObjs[operationID+"_providerCount"]+1);
	
    tb2  = document.createElement('table');
    for(var i = 0; i < 5; i++){
        var tr = tb2.insertRow();
        for(var j = 0; j < 2; j++){           
            var td = tr.insertCell();
            if(j==0) {
            	td.style.width = '30%';
            }
            if( i == 0 && j == 0) {
            	td.appendChild(document.createTextNode('Upload SQL Input Argument Transform File: '));
            }
            
            if( i == 0 && j == 1) {            
            	var  uploadSec1 = createUploadSection(idPrefix, "SQLInputArgumentTransFile");
            	td.appendChild(uploadSec1);
            }
            
            if(i == 1 && j == 0) {
            	td.appendChild(document.createTextNode('Upload SQL Result Transform File'));
            }
            
            
            if(i == 1 && j == 1) {
            	var uploadSec2 = createUploadSection(idPrefix, "SQLResultTransformFile");
            	
            	
            	td.appendChild(uploadSec2);
            }
            
            
            if(i == 2 && j == 0) {
            	td.appendChild(document.createTextNode('SQL Data Source Name Key'));
            }
            
            if(i == 2 && j == 1) {
            	var input = document.createElement("input");
            	input.setAttribute("id", idPrefix + "_sqlDataSourceName");
            	input.style = "width: 50.25em";
            	input.type = "text";
              	input.value = operationID+"_sql_datasource_name_" +(globalVar.operationObjs[operationID+"_providerCount"]+1);
            	input.disabled = true;
            	td.appendChild(input);
            	
          
            }
            
            if(i == 3 && j == 0) {
            	td.appendChild(document.createTextNode('SQL Statement Key'));
            }
            
            if(i == 3 && j == 1) {
            	var input = document.createElement("input");
            	input.type = "text";
            	input.style = "width: 50.25em";
            	input.setAttribute("id", idPrefix + "_sqlStatement");
            	input.value = operationID+"_sql_statement_" +(globalVar.operationObjs[operationID+"_providerCount"]+1);
            	input.disabled = true;
            	td.appendChild(input);
            }
            
            if(i == 4 && j == 0) {
            	td.appendChild(document.createTextNode('Routing Rule'));
            }
            
            if(i == 4 && j == 1) {
            	var input = document.createElement("input");
            	input.type = "text";
            	input.style = "width: 50.25em";
            	input.setAttribute("id", idPrefix + "_routingRule");
            	td.appendChild(input);
            }
        }
    }
    providerDetail.appendChild(tb2);
    
    providerDetail.setAttribute("id",  operationID +"_providerDetailSection_" + (globalVar.operationObjs[operationID+"_providerCount"]+1));
    globalVar.operationObjs[operationID+"_providerCount"] = globalVar.operationObjs[operationID+"_providerCount"] + 1;
    return providerDetail;
};

function createOptTableRecords(operationListObj) {
	var records = globalVar.operationRecord;
	for(key in operationListObj) {
		var record = {id: operationListObj[key],
				      operation: operationListObj[key]};
		    records.push(record);
		  
	}	
	globalVar.operationRecords = records;

	
	var frh_detail_sec_obj = document.getElementById("frontside_handler_sec");
	frh_detail_sec_obj.style.display = "block";
	var operation_config_sec_obj = document.getElementById("operation_config_sec");
	operation_config_sec_obj.style.display = "block";
	disaplyOperationDetailSection();
};

YUI().use('event', function (Y) {  
	var addButtonID = "fsh_addButton";
	Y.one("#"+addButtonID).on('click', handleFshAddBTNClick);
	function handleFshAddBTNClick(e) {
		var newSec = createFrontSideHandlerSec();
		var parentSec = Y.one("#"+"frh_detail_sec");
		var addButtonId = Y.one("#"+addButtonID);
		parentSec.insertBefore(newSec, addButtonId);
		globalVar.frontSideHandlerCount = globalVar.frontSideHandlerCount + 1;
	}
	
	Y.one("#"+"fsh_removeButton").on('click', handleFshRemoveBTNClick);
	function handleFshRemoveBTNClick(e) {
		if(globalVar.frontSideHandlerCount > 0) { 
			var divID = "fsh_detail_" + globalVar.frontSideHandlerCount;
			var divIDSec = document.getElementById(divID);    		
			divIDSec.remove();
			globalVar.frontSideHandlerCount = globalVar.frontSideHandlerCount - 1;
		}
	}
});	

function disaplyOperationDetailSection() {	
	
	YUI().use('datatable', function (Y) {
		var records = globalVar.operationRecords;
	    var table = new Y.DataTable({
	    	
	        columns: [
	            { key: "id",
	              formatter: '<input type="checkbox" name="item" value="{value}">',
	              label: 'Select',
	              allowHTML: true
	            },
	         
	            { key: "operation", label: "Operation Name"  }
	        ],
	        data: records        
	       
	    }).render("#dtable");
	    
	    function getOperationNameById(operationID) {
	    	for(var i = 0; i < records.length; i++) {
	    		if(records[i].id == operationID) {
	    			return records[i].operation;
	    		}
	    	}
	    	
	    	return null;
	    	
	    };

	    table.delegate('click', function (e) {    
	        var operationID = e.currentTarget.get("value");        
	        var checkBox = e.currentTarget;
	        if (checkBox.get("checked")) { 
	        	var operationName = getOperationNameById(operationID);
	        	tableCreate(operationName, operationID);
	        	globalVar.selectedOperation.push(operationID);
	        
	        } else {
	        	var operationIDDiv= document.getElementById(operationID);
	        	if(operationIDDiv != null) {
	        		var addButtonID = operationID+"_addbutton";
	        	    var toggleButtonID = operationID+"_toggleButton";
	        		Y.one("#"+toggleButtonID).detach("click");
	        		Y.one("#"+addButtonID).detach("click");
	        	    operationIDDiv.remove();	        	    
	        	}
	        	
	        	var selectedOps = globalVar.selectedOperation;
	        	  for(var i = selectedOps.length; i--;) {
	                  if(selectedOps[i] === operationID) {
	                	  selectedOps.splice(i, 1);
	                  }
	              }
	        	  globalVar.operationObjs[operationID+"_testDetailCount"] = 0;
	        	  globalVar.operationObjs[operationID+"_providerCount"] = 0;
	        }
	     
	        
	    }, '.yui3-datatable-data input', table);

	});
	
};

YUI().use('event', function (Y) {  
	var nextButton = "next_Button";
	Y.one("#"+nextButton).on('click', handleClick);
	function handleClick(e) {	
		 
        YUI().use('io', "json-parse", "json-stringify", function (Y) {
        	var cfg,
            request;
        	 try {
        		 var serviceData = collectServiceData();
        	 } catch(e) {
        		 var errorSecEle =  document.getElementById("generic_error_msg_sec");
        		 errorSecEle.innerHTML = "<font size='4' color='red'>Information on this page is missing and/or not valid. Please scroll up the page to correct the items marked as 'Error'.</font> <br/><br/>";
        		 errorSecEle.style.display = 'block';
        		
        		 return;
        	 }
        	cfg = {
        			method: 'POST',
        	        sync: true,
        	        data:  Y.JSON.stringify(serviceData),
        	        headers: {
        	        	'Accept': 'application/json',
        	        	'Content-Type': 'application/json'
        	          },
        	        arguments: { 'foo' : 'bar' }
        	    };
        	
        	function onSuccess(transactionid, response, arguments) {
        		  try {
                      var messages = Y.JSON.parse(response.responseText);
                      window.location.href="/sf_ide/environmentProperties"
                      
                  }
                  catch (e) {
                      alert("JSON Parse failed!");
                      return;
                  }
                 
        	}

        		// Subscribe to "io.success".
        	Y.on('io:success', onSuccess, Y, true);
        	request = Y.io("/sf_ide/processServiceInfo", cfg);
        });
		
	}
});

function collectServiceData() {
	var serviceDetail = {
			serviceName: "",
			serviceTargetNameSpace: "",
			businessDomain: "",
			operationDetails: [],
			frontSideHandlers: []
	};
	
	var businessDomainId="business_domain";
	var businessDomainEle = document.getElementById(businessDomainId);
	serviceDetail.businessDomain = businessDomainEle.value;
	collectOperationData(serviceDetail);
	collectFrontSideHandlerData(serviceDetail);
	return serviceDetail;
}

function collectFrontSideHandlerData(serviceDetail) {
	var prefix = "fsh_detail_";
	for(var i = 1; i <= globalVar.frontSideHandlerCount; i++) {
		var nameID = prefix+ i + "_name";
		var nameElement = document.getElementById(nameID);
		var nameValue = nameElement.value;
		
		var typeID = prefix+i + "_type";
		var typeElement = document.getElementById(typeID);
		var optionTypeValue = typeElement.value;
		var typeValue =  optionTypeValue.substring(optionTypeValue.indexOf("_") + 1);;
		
		var fronsideHandler = {
				name: nameValue,
				type: typeValue
		}
		serviceDetail.frontSideHandlers.push(fronsideHandler);
	}
}

function collectOperationData(serviceDetail) {
	
	var selectedOps =	globalVar.selectedOperation;
	
	for(var i = selectedOps.length; i--;) {
		var operationDetail = {
				operationName: "",
				pattern: "",
				compositeServiceFlag: "false",
				providers: [],
				testConfigs:[]
		};
		
		operationDetail.operationName = selectedOps[i];
		var operationID  = operationDetail.operationName;
		var patternID = operationID + "_patternID";
		var patternElement = document.getElementById(patternID);		
		var optionValue = patternElement.value;
		
		var radiobtn = document.getElementById("compositeService_yes");
		if(radiobtn.checked) {
			operationDetail.compositeServiceFlag = "true";
		} else {
			operationDetail.compositeServiceFlag = "false";
		}
		
		
		operationDetail.pattern = optionValue.substring(optionValue.indexOf("_") + 1);
		
		collectProviderDetail(operationDetail, operationID, operationDetail.pattern );
		collectTestConfiguration(operationDetail, operationID);
		serviceDetail.operationDetails.push(operationDetail);
    }
	
}

function collectProviderDetail(operationDetail, operationID, pattern) {
  	var providerNumber = globalVar.operationObjs[operationID+"_providerCount"];
  	
  	for(var i = 1 ; i <= providerNumber; i++) {
  		provider = {
  				inputTransformation: "",
  				outputTransformation: "",
  				backendUrl: "",
  				routingRule: "",	

  				sqlInputArgumentsTransFileName: "",
  				sqlResultsetTransformFileName: "",
  				sqlDatasourceNameKey: "",
  				sqlStatementKey: ""
  		};
  		var idPrefix = operationID + "_"+i;
  		
  		var routingRule_id = idPrefix + "_routingRule";
		var routingRuleEle = document.getElementById(routingRule_id);
		provider.routingRule =  routingRuleEle.value; 	
  		
  		if(pattern === "binary" || pattern == "versiongateway" ) {
  			var reqDPA_msg_div_id =  idPrefix + "_reqDPA" + "_msgContainer";
  			var reqDPA_element = document.getElementById(reqDPA_msg_div_id);
  			if(reqDPA_element.firstChild == null) {
  				if( pattern == "binary") {
  					reqDPA_element.innerHTML="<font size='3' color='red'>Error: Please upload the Request DPA file</font>";
  					throw new Error("error");
  				}
  			} else {
  				provider.inputTransformation = reqDPA_element.firstChild.textContent;
  			}  			
  			
  			var resDPA_fileName_id =  idPrefix + "_resDPA"+"_msgContainer";
 			var resDPA_element = document.getElementById(resDPA_fileName_id);
 			
 			if(resDPA_element.firstChild == null) {
 				if( pattern == "binary") {
  					reqDPA_element.innerHTML="<font size='3' color='red'>Error: Please upload the Response DPA file</font>";
  					throw new Error("error");
  				} 
 			} else {
					provider.outputTransformation = resDPA_element.firstChild.textContent;
			}
  			
  			
  			var backEndURL_id = idPrefix + "_backEndUrl";
  			var backEndUrlEle = document.getElementById(backEndURL_id);
  			provider.backEndUrlKey =  backEndUrlEle.value;  			
  			
  		} else {
  			var sqlInputArgumentTransFileNameId = idPrefix+"_SQLInputArgumentTransFile" + "_msgContainer";
  			var sqlInputArgumentTransFileName_element = document.getElementById(sqlInputArgumentTransFileNameId);
  			provider.sqlInputArgumentsTransFileName = sqlInputArgumentTransFileName_element.firstChild.textContent;
  			
  			var sqlResultTransformFileNameId = idPrefix+"_SQLResultTransformFile" + "_msgContainer";
  			var sqlResultTransformFileName_element = document.getElementById(sqlResultTransformFileNameId);
  			provider.sqlResultsetTransformFileName = sqlResultTransformFileName_element.firstChild.textContent;
  			
  			var dataSourceID = idPrefix + "_sqlDataSourceName"
  			var dataSourceID_element = document.getElementById(dataSourceID);
  			provider.sqlDatasourceNameKey = dataSourceID_element.value;
  			
  			var sqlStatement = idPrefix + "_sqlStatement"
  			var sqlStatement_element = document.getElementById(sqlStatement);
  			provider.sqlStatementKey = sqlStatement_element.value;
  		}
  		
  		operationDetail.providers.push(provider);
  	}
}

function collectTestConfiguration(operationDetail, operationID) {
	var testConfigNumber = globalVar.operationObjs[operationID+"_testDetailCount"];
	
	for(var i = 1 ; i <= testConfigNumber; i++) {
		var testConfiguration = {
				sampleSoapRequestFileName: "",
				responseFileName: "",
				latency: "",
				routingRule: ""
		};
		var idPrefix = operationID + "_" + i;
		
		var sampeReqFileId = idPrefix +"_sampleReqFile"+"_msgContainer";
		var sampeReqFile_element = document.getElementById(sampeReqFileId);
		if(sampeReqFile_element.firstChild !== null) {
			testConfiguration.sampleSoapRequestFileName = sampeReqFile_element.firstChild.textContent;
		}
				
		var respFileId = idPrefix +"_respFile"+"_msgContainer";
		var respFile_element = document.getElementById(respFileId);
		
		if(respFile_element.firstChild !== null) {
			testConfiguration.responseFileName = respFile_element.firstChild.textContent;
		}
		var latencyId = idPrefix+"_latency"
		var latency_element = document.getElementById(latencyId);
		testConfiguration.latency = latency_element.value;
		
		var routingRule_id = idPrefix + "_routingRule";
		var routingRuleEle = document.getElementById(routingRule_id);
		testConfiguration.routingRule =  routingRuleEle.value; 	
		
		operationDetail.testConfigs.push(testConfiguration);
		
	}
	 
}
