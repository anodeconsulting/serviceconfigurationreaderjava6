<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>

<title>Next Pathway - Service Factory</title>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.18.1/build/cssbase/cssbase-min.css">
<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.18.1/build/cssgrids/cssgrids-min.css">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/main-min.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/docs-min.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/prettify-min.css'/>">
<script type="text/javascript" src="http://yui.yahooapis.com/3.18.1/build/yui/yui-min.js" > </script>
<script type="text/javascript" src="<c:url value='/resources/js/environment.js'/>" > </script>

</head>
<body class="yui3-skin-sam">
<style>
#root_wsdl_entry {
    margin-top: 15px;
}

</style>
	<div id="doc">
		<div id="hd" class="yui3-g">
			<header>
				<div class="content">
					<div class="yui3-u-3-4" align="center">
						<font size="8" color="black">Environment Properties</font>
					</div>


				</div>
			</header>
		</div>
		<div class="yui3-u-1">
			<div id="docs-hd" class="content">
				<h1></h1>


			</div>
		</div>
		<div class="yui3-u-3-4 print-max-width">
			<div id="docs-main" class="content">
				<div class="example yui3-skin-sam">
					<div>
						<table style="width: 100%">
							<tr>
								<td>Project Name:</td>
								<td><input type="text" name="project_Name"
									id="project_Name_id" value="Leap"></td>
							</tr>
							
							<tr>
								<td>Domain Name:</td>
								<td><input type="text" name="domain_name"
									id="domain_name_id" value="lrsdomain_8"></td>
							</tr>
							<tr>
								<td>Repository URL:</td>
								<td><input type="text" name="repository_url"
									id="repository_url_id" value="local:///"></td>
							</tr>

							<tr>
								<td>Should Flush Cache:</td>
								<td><input type="radio" name="flush_cache" value="True"
									id="flush_cache_true_id" checked>True <input type="radio"
									name="flush_cache" value="False" id="flush_cache_false_id">False</td>
							</tr>
						</table>

					</div>
				</div>
				<div id="environment_specific_sec" class="content">
					<div class="example yui3-skin-sam">
						<div id="environment_type_sec">
							
						</div>
						<br/>
						
					</div> 
				</div>


				<div style="margin-bottom: 40px;">
					<button type="button" id="next_Button">Next</button>
			</div>
		</div>
	</div>
	</div>
</body>
</html>