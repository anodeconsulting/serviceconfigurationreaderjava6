<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>

<title>Next Pathway - Service Factory</title>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.18.1/build/cssbase/cssbase-min.css">
<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.18.1/build/cssgrids/cssgrids-min.css">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/main-min.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/docs-min.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/prettify-min.css'/>">
<script type="text/javascript" src="http://yui.yahooapis.com/3.18.1/build/yui/yui-min.js" > </script>



</head>
<body class="yui3-skin-sam">
<style>
#root_wsdl_entry {
    margin-top: 15px;
}

</style>
	<div id="doc">
		<div id="hd" class="yui3-g">
			<header>
				<div class="content">
					<div class="yui3-u-3-4" align="center">
						<font size="8" color="black">Download Generated Service Project</font>
					</div>


				</div>
			</header>
		</div>
		<div class="yui3-u-1">
			<div id="docs-hd" class="content">
				<h1></h1>


			</div>
		</div>
		<div class="yui3-u-3-4 print-max-width">
			<div id="docs-main" class="content">
				<div class="example yui3-skin-sam">
					<div id="root_wsdl_entry">
						<form method='GET' action='/sf_ide/downloadFile'>  
						    <table style="width: 100%">
							<tr>
								<td><img src="<c:url value='/resources/images/service_project_zip_file.jpg'/>" alt="Smiley face" width="42" height="42"/>
								 <br/> <br/> <input type="submit" value="Download Service Project"></td>
								
							</tr>
							
						</table>								
    						
   							 
 					    </form>
					</div>
				</div>
				<div style="margin-bottom: 40px;">
					<button type="button" id="back_Button" onclick="document.location='/sf_ide/environmentProperties';">Back</button>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>
</html>