<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Crunchify - Upload Multiple Files Example</title>
<style type="text/css">

</style>
</head>
<body>
  <div align="center"><h1><spring:message code="title" /></h1></div>
    <br>
    <br>
    <div align="center">       
        <p>Awesome.. Following files are uploaded successfully.</p>
        <ol>
            <c:forEach items="${uploadedFiles}" var="file">
           - ${file} <br>
            </c:forEach>
        </ol>
        <a href="<%=request.getContextPath()%>/upload"><input
            type="button" value="Go Back" /></a> <br />
        <br />
        <br />
       
    </div>
</body>
</html>