<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
 
<title>Next Pathway - Service Factory</title>
<meta charset="UTF-8">
<script type="text/javascript"
	src="http://yui.yahooapis.com/3.18.1/build/yui/yui-min.js"> </script>
<link rel="stylesheet" type="text/css"
	href="http://yui.yahooapis.com/3.18.1/build/cssbase/cssbase-min.css">
<link rel="stylesheet" type="text/css"
	href="http://yui.yahooapis.com/3.18.1/build/cssgrids/cssgrids-min.css">
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/main-min.css'/>"> 
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/docs-min.css'/>">
<link rel="stylesheet" type="text/css"
	href="<c:url value='/resources/css/prettify-min.css'/>">

<script type="text/javascript"
	src="<c:url value='/resources/js/service_config.js'/>"> </script>

</head>
<body class="yui3-skin-sam">
	<style>
#filelist, #root_wsdl_entry {
	margin-top: 15px;
}

#uploadFilesButtonContainer, #selectFilesButtonContainer,
	#overallProgress {
	display: inline-block;
}

#overallProgress {
	float: right;
}
</style>
	<div id="doc">
		<div id="hd" class="yui3-g">
			<header>
				<div class="content">
					<div class="yui3-u-3-4" align="center">
						<font size="8" color="black">Service Configuration</font>
					</div>


				</div>
			</header>
		</div>
		<div class="yui3-u-1">
			<div id="docs-hd" class="content">
				<h2>Upload WSDL</h2>

			</div>
		</div>
		<div class="yui3-u-3-4 print-max-width">
			<div id="docs-main" class="content">
				<div class="example yui3-skin-sam">
					<div id="uploaderContainer">
						<div id="selectFilesButtonContainer"></div>
						<div id="uploadFilesButtonContainer">
							<button type="button" id="uploadFilesButton" class="yui3-button"
								style="width: 250px; height: 35px;">Upload Files</button>
						</div>


						<div id="overallProgress"></div>
					</div>
					
					<div id="filelist">
						<table id="filenames">
							<thead>
								<tr>
									<th>File name</th>
									<th>File size</th>
									<th>Percent uploaded</th>
								</tr>
								<tr id="nofiles">
									<td colspan="3">No files have been selected.</td>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div id="root_wsdl_entry"  style="display: none">
						<table style="width: 100%" >
							<tr>
								<td>Select Service WSDL File:</td>
								<td><select id="wsdlList" >
								        <option value="0" >Choose Service WSDL File</option>
									</select></td>
							</tr>
						</table>
					</div>

				</div>
			</div>
			<div id="frontside_handler_sec" class="content" style="display: none">

				<div>
					<h2>Front Side Handler Configuration</h2>
				</div>

				<br />
				<div class="example yui3-skin-sam">

					<div id="fshSec" class="content">

						<div id="fshSec_title">
							<table style="width: 100%">
								<tr>
									<td>Front Side Handler</td>
									<td><button type="button" id="fsh_toggle_btn">Front
											Side Handler Detail Toggle Button</button></td>
								</tr>
							</table>
						</div>


						<div
							style="border-radius: 1px; box-shadow: 0 0 3px #000000; padding: 1em;"
							id="frh_detail_sec">
							<div id="fsh_detail_1">
								<label>Front Side Handler Detail</label>
								<table style="width: 100%">
									<tr>
										<td>Name:</td>
										<td><input type="text" name="fsh_name"
											id="fsh_detail_1_name"></td>
									</tr>
									<tr>
										<td>Type:</td>
										<td><select id="fsh_detail_1_type">
												<option value="1_HTTPS">HTTPS Source Protocol
													Handler</option>
												<option value="1_HTTP">HTTP Source Protocol Handler</option>
										</select></td>
									</tr>
								</table>
							</div>

							<button type="button" id="fsh_addButton">Add</button>
							<button type="button" id="fsh_removeButton">Remove</button>
						</div>
					</div>
					<br /> <br />					
				</div>
			</div>
			<div id="operation_config_sec" class="content" style="display: none">

				<div>
					<h2>Operation Configuration</h2>
				</div>

				<br />
				<div class="example yui3-skin-sam">

                   <div id="businessDomain" style="border-radius: 1px; box-shadow: 0 0 3px #000000; padding: 1em;"><table style="width: 100%">
								<tr>
									<td>Business Domain</td>
									<td><input type="text" id="business_domain" style= "width: 30.25em"></input></td>
								</tr>
								<tr>
									<td>Is Composite Service</td>
									<td><input type="radio" name="compositeServiceFlag"  id="compositeService_yes" value="Y" /> Yes 
									    <input type="radio" name="compositeServiceFlag"  id="compositeService_no" checked  value="N"/> No </td> 
								</tr>
							</table></div>
							
					<br />

					<div id="dtable"></div>

					<br /> <br />

					<div id="operationDetail"></div>

				</div>
				<br /> <br />
			</div>
            <div id="generic_error_msg_sec" style="display: none">
            </div>
			<div style="margin-bottom: 40px;">
				<button type="button" id="next_Button">Next</button>
			</div>
		</div>
	</div>

</body>
</html>