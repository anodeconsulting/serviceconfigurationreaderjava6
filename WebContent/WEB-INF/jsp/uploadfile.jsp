<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<title>Next Pathway - Service Factory</title>
 <link href="<c:url value="/resources/css/corp.css" />" rel="stylesheet">
<script
    src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script>
    $(document)
            .ready(
                    function() {
                        //add more file components if Add is clicked
                        $('#addFile')
                                .click(
                                        function() {
                                            var fileIndex = $('#fileTable tr')
                                                    .children().length - 1;
                                            $('#fileTable')
                                                    .append(
                                                            '<tr><td>'
                                                                    + '   <input type="file" name="files['+ fileIndex +']" />'
                                                                    + '</td></tr>');
                                        });
 
                    });
</script>

</head>
<body>

     <div align="center"><h1><spring:message code="title" /></h1></div>
      <div align="center">
        <form:form method="post" action="uploadWSDL"
            modelAttribute="uploadForm" enctype="multipart/form-data">
           
            <p>Select WSDL files to upload</p>
         
            <table id="fileTable">
                <tr>
                    <td><input name="files[0]" type="file" /></td>
                </tr>         
                <tr>
                 <td> <input type="submit" value="Next" /> </td>
                </tr>      
            </table>
            <br />
             
        </form:form>     
        </div>
</body>
</html>